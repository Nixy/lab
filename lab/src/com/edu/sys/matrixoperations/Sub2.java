package com.edu.sys.matrixoperations;


import com.edu.sys.matrix.Matrix;

/**
 * Created with IntelliJ IDEA.
 * User: Nixy
 * Date: 26.10.13
 * Time: 14:49
 * To change this template use File | Settings | File Templates.
 */
public class Sub2 {
    public static Matrix calc(Matrix... args){
        if (args[0] != null && args[1] != null)
            if (args[0].isSameSize(args[1])){
                Matrix result = new Matrix().copy(args[0]);
                for (int i = 0 ; i < result.getRow(); i++)
                    for ( int j = 0 ; j < result.getCol(); j++)
                        result.setData(i,j,result.getData(i,j)-args[1].getData(i,j));
                return result;
            }
        return null;
    };
}
