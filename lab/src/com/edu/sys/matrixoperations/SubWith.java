package com.edu.sys.matrixoperations;


import com.edu.sys.matrix.Matrix;

/**
 * Created with IntelliJ IDEA.
 * User: Nixy
 * Date: 26.10.13
 * Time: 14:49
 * To change this template use File | Settings | File Templates.
 */
public class SubWith {
    public static Matrix calc(Matrix... args){
        if (args[0] != null && args[1] != null)
            if (args[0].isSameSize(args[1])){
                for (int i = 0 ; i < args[0].getRow(); i++)
                    for ( int j = 0 ; j < args[0].getCol(); j++)
                        args[0].setData(i,j,args[0].getData(i,j)-args[1].getData(i,j));
                return args[0];
            }
        return null;
    };
}
