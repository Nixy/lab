package com.edu.sys.matrixoperations;


import com.edu.sys.matrix.Matrix;

/**
 * Created with IntelliJ IDEA.
 * User: Александра
 * Date: 10.10.13
 * Time: 20:08
 * To change this template use File | Settings | File Templates.
 */
public class MinorInverse {
    public static final Matrix calc (Matrix...args){
        Matrix result = new Matrix().copy(args[0]);
        double determ = Determinant.calc(args[0]);

        if (determ != 0){
            for (int i = 0 ; i < args[0].getCol() ; i++)
                for( int j = 0 ; j < args[0].getRow(); j++){
                    Matrix tmp = new Matrix(args[0].getCol() - 1, args[0].getCol() - 1);
                    for ( int n = 0 ,x = 0; x < args[0].getCol() ; x++){
                        for ( int m = 0 ,y = 0 ; y < args[0].getCol() ; y++)
                            if (x != i & y != j){
                                tmp.setData(n, m, args[0].getData(x, y));
                                m++;
                            }
                        if (x != i )
                            n++;
                    }

                    result.setData(i, j,Determinant.calc(tmp));
                }

            result = new Transpose().calc(result);

            for(int i = 0 ; i < result.getRow() ; i++)
                for (int j = 0 ; j < result.getCol(); j++)
                    result.setData(j, i,(float)(result.getData(j, i)*Math.pow(-1, i+j)));


            for(int i = 0 ; i < result.getRow() ; i++)
                for (int j = 0 ; j < result.getCol(); j++)
                    result.setData(j, i,(float)(result.getData(j, i)/determ));


            return result;
        }
        else return null;
    }

}
