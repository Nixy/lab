package com.edu.sys.matrixoperations;


import com.edu.sys.matrix.Matrix;

/**
 * Created with IntelliJ IDEA.
 * User: Nixy
 * Date: 26.11.13
 * Time: 20:24
 * To change this template use File | Settings | File Templates.
 */
public class GaussInv  {
    public static Matrix calc(Matrix m){
        if (m.getRow() == m.getCol()){
            Matrix E = new Matrix(m.getRow(),m.getRow());
            Matrix tmp = new Matrix(m);
            for (int i = 0; i < E.getRow(); i++) E.setData(i,i,1);

            for (int i = 0; i < m.getRow(); i++){
                double div = tmp.getData(i,i);
                if (div != 0){
                    for (int j = 0 ; j < m.getCol(); j++ ){
                        E.setData(i,j,E.getData(i,j)/div);
                        tmp.setData(i,j,tmp.getData(i,j)/div);
                    }
                    for (int k = 0; k < m.getRow(); k++){
                        if (k!=i) {
                            double mult = tmp.getData(k,i);
                            for (int j = 0; j < m.getCol()  ; j++){
                                E.setData(k,j,E.getData(k,j)-E.getData(i,j)*mult);
                                tmp.setData(k,j,tmp.getData(k,j)-tmp.getData(i,j)*mult);
                            }
                        }
                    }
                } else {
                    for ( int k = i+1; k < m.getRow(); k++){
                        if (( div = tmp.getData(k,i) ) != 0){
                            swap(k,i,tmp);
                            swap(k,i,E);
                            i--;
                            k = m.getRow();
                        }
                    }
                    if (div == 0)
                        return null;
                };
            }

            return E;
        }
        return null;
    }

    static void swap(int nw, int prv,Matrix a){
        for (int i = 0; i < a.getCol(); i++){
            double tmp = a.getData(prv,i);
            a.setData(prv,i,a.getData(nw,i));
            a.setData(nw,i,tmp);
        }
    }

}
