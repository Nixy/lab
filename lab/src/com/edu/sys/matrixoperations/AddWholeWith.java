package com.edu.sys.matrixoperations;


import com.edu.sys.matrix.Matrix;

/**
 * Created with IntelliJ IDEA.
 * User: Nixy
 * Date: 22.09.13
 * Time: 14:15
 *
 * Класс AddWholeWIth
 * Сложение всех матриц из args и сохранение результата в первой из них
 */
public class AddWholeWith   {
    public static Matrix calc(Matrix... args){
        boolean flag = true;
        for (int n = 0 ; n < args.length ; n ++)
            flag = flag && args[n] != null;
        if (flag){
            for (int n = 0 ; n < args.length-1 ; n ++)
                flag = flag && args[n].isSameSize(args[n+1]);
            if (flag){
                for (int n = 1 ; n < args.length ; n ++)
                    for (int i = 0 ; i < args[0].getRow(); i++)
                        for ( int j = 0 ; j < args[0].getCol(); j++)
                            args[0].setData(i,j,args[0].getData(i,j)+args[n].getData(i,j));
                return args[0];
            }
        }
        return null;
    };
}
