package com.edu.sys.matrixoperations;


import com.edu.sys.matrix.Matrix;

/**
 * Created with IntelliJ IDEA.
 * User: Александра
 * Date: 10.10.13
 * Time: 19:59
 * To change this template use File | Settings | File Templates.
 */
public class MultiWith {
    public static Matrix calc(Matrix... args){
        if (args[0].getCol() == args[1].getRow()){
            Matrix result = new Matrix(args[0].getRow(), args[1].getCol());
            for (int i=0; i<args[1].getCol(); i++)
                for (int j=0; j<args[0].getRow(); j++){
                    double summ = 0;
                    for (int k=0; k < args[0].getCol(); k++) {
                        double a =  args[0].getData(j,k);
                        double b = args[1].getData(k,i);
                        summ = summ + a*b;
                    }
                    result.setData(j,i,summ);
                }
            return  args[0].copy(result);
        }
        return null;
    };

}

