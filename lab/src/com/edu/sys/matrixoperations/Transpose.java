package com.edu.sys.matrixoperations;


import com.edu.sys.matrix.Matrix;

/**
 * Created with IntelliJ IDEA.
 * User: Александра
 * Date: 24.09.13
 * Time: 13:27
 * To change this template use File | Settings | File Templates.
 */
public class Transpose {
    public static Matrix calc(Matrix... args){
            Matrix result = new Matrix(args[0].getCol(),args[0].getRow());
            for (int i=0; i < args[0].getRow(); i++ )
                for (int j=0; j < args[0].getCol(); j++)
                    result.setData(j,i, args[0].getData(i,j));
            return  result;


    };

}
