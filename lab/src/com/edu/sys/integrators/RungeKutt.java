package com.edu.sys.integrators;


import com.edu.sys.models.AbstractModel;

import com.edu.sys.matrix.Matrix;
import com.edu.sys.matrixoperations.*;


/**
 * Created with IntelliJ IDEA.
 * User: Nixy
 * Date: 20.09.13
 * Time: 7:32
 * To change this template use File | Settings | File Templates.
 */
public class RungeKutt extends AbstractIntegrator {

    Matrix K1,K2,K3,K4;
    Matrix Y0,Y,tmp;

    public RungeKutt(AbstractModel m, double start, double end, double step) {
        super(m, start, end, step);
        K1 = new Matrix();
        K2 = new Matrix();
        K3 = new Matrix();
        K4 = new Matrix();
        Y0 = new Matrix();
        tmp = new Matrix();
        Y = new Matrix().copy(m.getX(m.getCount()-1));

    }

    @Override
    public void integrate() {
       long time = System.nanoTime();
       for (double i = start ; i < end ; i+=step){

           Y0.copy(Y);

           tmp.copy(Y0);
           K1.copyWithFactor(model.F(i, tmp), step);

           tmp.copyWithFactor(K1,0.5).copy(AddWith.calc(tmp, Y0));
           K2.copyWithFactor(model.F(i+step/2,tmp),step);

           tmp.copyWithFactor(K2,0.5).copy(AddWith.calc(tmp, Y0));
           K3.copyWithFactor(model.F(i+step/2,tmp),step);

           tmp.copy(K3).copy(AddWith.calc(tmp, Y0));
           K4.copyWithFactor(model.F(i+step,tmp),step);

           Y.copy(AddWholeWith.calc(Y0,
                   K1.copyWithFactor(K1, 1. / 6),
                   K2.copyWithFactor(K2, 1. / 3),
                   K3.copyWithFactor(K3, 1. / 3),
                   K4.copyWithFactor(K4, 1. / 6)));

           model.setX(new Matrix(Y));

           if (model.isEnough(step))
               i = end;
       }
    }
}
