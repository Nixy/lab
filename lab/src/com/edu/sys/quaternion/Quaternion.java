package com.edu.sys.quaternion;

import com.edu.sys.matrix.Matrix;
import com.edu.sys.vectoroperations.CrossProduct;

import static java.lang.Math.*;

/**
 * Author Grinch
 * Date: 30.06.13
 * Time: 13:01
 */
public class Quaternion {
    public static final int TYPE_ROW = 0;
    public static final int TYPE_COL = 1;
    double[] vec;

    public Quaternion(double... args){
        vec = new double[4];
        for (int i=0;i<4;i++){
            vec[i] = args[i];
        }
    }

    public Quaternion(double alpha,Matrix e){
        vec = new double[]{cos(alpha/2),
                sin(alpha/2)*e.getData(0,0),
                sin(alpha/2)*e.getData(1,0),
                sin(alpha/2)*e.getData(2,0)};

    }

    public Quaternion conjugate(){
        return new Quaternion(vec[0],-vec[1],-vec[2],-vec[3]);
    }

    public Quaternion multiplication(Quaternion q1){
        double[] newvec = new double[4];
        newvec[0] = 0;
        newvec[0] += vec[0]*q1.getReal();
        Matrix vm = new CrossProduct().calc(this.getIm(TYPE_ROW),q1.getIm(TYPE_ROW));
        for (int i=0;i<3;i++){
            newvec[0] -= q1.getIm(TYPE_ROW).getData(i, 0)*vec[i+1];
            newvec[i+1] = vm.getData(i, 0)+vec[0]*q1.getIm(TYPE_ROW).getData(i, 0)+q1.getReal()*vec[i+1];
        }

        return new Quaternion(newvec);
    }

    public Quaternion multiplication(Matrix v){
        if (v.getCol() > 1){
            return multiplication(new Quaternion(0,v.getData(0, 0),v.getData(0, 1),v.getData(0, 2)));
        }else{
            return multiplication(new Quaternion(0,v.getData(0, 0),v.getData(1, 0),v.getData(2, 0)));
        }

    }

    public double getReal(){
        return vec[0];
    }

    public  double module(){
        Quaternion q = multiplication(conjugate());
        return sqrt(q.getReal());
    }

    public Quaternion norm(){
        double d = module();
        double[] v = new  double[]{vec[0]/d,vec[1]/d,vec[2]/d,vec[3]/d};
        return new Quaternion(v);
    }

    public Matrix toMatrix(){
        /*
        |       2     2                                |
        | 1 - 2Y  - 2Z    2XY - 2ZW      2XZ + 2YW     |
        |                                              |
        |                       2     2                |
    M = | 2XY + 2ZW       1 - 2X  - 2Z   2YZ - 2XW     |
        |                                              |
        |                                      2     2 |
        | 2XZ - 2YW       2YZ + 2XW      1 - 2X  - 2Y  |
        |                                              |
        */
        double w = vec[0];
        double x = vec[1];
        double y = vec[2];
        double z = vec[3];
        return new Matrix(new double[][]{{1-2*y*y-2*z*z,    2*x*y-2*z*w,    2*x*z+2*y*w},
                                         {2*x*y+2*z*w,  1-2*x*x-2*z*z,  2*y*z-2*x*w},
                                         {2*x*z-2*y*w, 2*y*z + 2*x*w,   1-2*x*x-2*y*y}});
    }

    public Matrix getIm(int type){
        Matrix result = null;
        switch (type){
            case TYPE_COL:
                result = new Matrix(1,3);
                for (int i=0;i<3;i++){
                    result.setData(0, i, vec[i + 1]);
                }
                break;
            case TYPE_ROW:
                result = new Matrix(3,1);
                for (int i=0;i<3;i++){
                    result.setData(i, 0, vec[i + 1]);
                }
        }
        return result;
    }

    @Override
    public String toString() {
        String result = "{" ;
        for (int i = 0 ; i < 4; i++){
            result += String.valueOf(vec[i])+ (i < 3 ? "," : "");
        }
        result += "} m ="+String.valueOf(module());
        return result;
    }
}
