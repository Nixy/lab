package com.edu.sys.models;

import com.edu.sys.matrix.Matrix;
import com.edu.sys.vectoroperations.Module;
import com.edu.sys.vectoroperations.Norm;

import static java.lang.Math.*;

/**
 * Created by Nixy on 03.05.14.
 */
public class SelfMotion extends AbstractModel {

    private static final double MAX_MV = 200*1000./3600 ;

    public SelfMotion(Matrix initCondition, double step) {
        super(initCondition, step);
    }

    @Override
    public boolean isEnough(Object... params) {
        return false;
    }

    @Override
    public Matrix F(double t, Matrix Xn) {
        double x = Xn.getData(0,0);
        double y = Xn.getData(1,0);
        double z = Xn.getData(2,0);

        double vx = Xn.getData(3,0);
        double vy = Xn.getData(4,0);
        double vz = Xn.getData(5,0);

        double ax = Xn.getData(6,0);
        double ay = Xn.getData(7,0);
        double az = Xn.getData(8,0);

        double xc = Xn.getData(9,0);
        double yc = Xn.getData(10,0);
        double zc = Xn.getData(11,0);


        Matrix V = new Matrix(new double[][]{{vx},{vy},{vz}});
        double mv = Module.calc(V);
        mv = mv > MAX_MV ? MAX_MV/mv : 1;
        V = V.getWithFactor(mv);
        double dx = V.getData(0,0);         double dy = V.getData(1,0);           double dz = V.getData(2,0);

        double dvx = ax;        double dvy = ay;          double dvz = az;

        double d = sqrt(pow(x - xc, 2) + pow(y - yc, 2) + pow(z - zc, 2));
        Matrix A = new Matrix(new double[][]{{(xc-x)/d},{(yc-y)/d},{(zc-z)/d}});

        A = Norm.calc(A);
        double dax = A.getData(0,0)*1.5;
        double day = A.getData(1,0)*1.5;
        double daz = A.getData(2,0)*1.5;

        double dxc = 0;          double dyc = 0;           double dzc = 0;


        Xn.setData(0,0,dx);
        Xn.setData(1,0,dy);
        Xn.setData(2,0,dz);
        Xn.setData(3,0,dvx);
        Xn.setData(4,0,dvy);
        Xn.setData(5,0,dvz);
        Xn.setData(6,0,dax);
        Xn.setData(7,0,day);
        Xn.setData(8,0,daz);
        Xn.setData(9,0,dxc);
        Xn.setData(10,0,dyc);
        Xn.setData(11,0,dzc);


        return Xn;
    }
}
