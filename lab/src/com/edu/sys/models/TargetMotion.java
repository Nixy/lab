package com.edu.sys.models;

import com.edu.sys.matrix.Matrix;
import com.edu.sys.matrixoperations.Add2;
import com.edu.sys.matrixoperations.Rotate;
import com.edu.sys.vectoroperations.CrossProduct;
import com.edu.sys.vectoroperations.Module;
import com.edu.sys.vectoroperations.Norm;
import com.edu.sys.vectoroperations.Scalar;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;

import static java.lang.Math.*;

/**
 * Created by Nixy on 30.04.14.
 */
public class TargetMotion extends AbstractModel {

    public static final double MAX_V =  300*1000./3600;
    private static final double T = 1;
    double[] params;
    HashMap<Double,double[]>  map ;

    public Matrix omega = new Matrix(new double[][]{{1},
                                                    {0},
                                                    {0}});
    Matrix Omega;
    public TargetMotion(Matrix initCondition, double step,HashMap<Double,double[]> map) {
        super(initCondition, step);
        this.map = map;
        Omega = getOmega(0);
    }

    @Override
    public boolean isEnough(Object... params) {
        return false;
    }

    @Override
    public Matrix F(double t, Matrix Xn) {
        double x = Xn.getData(0,0);
        double y = Xn.getData(1,0);
        double z = Xn.getData(2,0);

        double vx = Xn.getData(3,0);
        double vy = Xn.getData(4,0);
        double vz = Xn.getData(5,0);

        Omega = getOmega(t);
        Matrix R = new Matrix(new double[][]{{x},{y},{z}});
        double h1 = Module.calc(R);
        Matrix D = Norm.calc(R);
        Matrix V = CrossProduct.calc(Omega, D);
        double cos = Scalar.calc(Omega,D);
        double r = h1 * ( sqrt(1- cos*cos));
        double dr = r - params[2];
        Matrix A = CrossProduct.calc(Omega,V);
        Matrix delta = A.getWithFactor(dr);

        V = CrossProduct.calc(Omega, R);
        V = Add2.calc(V,delta);

        double mv = Module.calc(V);
        mv = mv > MAX_V ? MAX_V/mv : 1;
        V = V.getWithFactor(mv);

        double dx = V.getData(0,0);
        double dy = V.getData(1,0);
        double dz = V.getData(2,0);

        double dvx = 0*A.getData(0,0);
        double dvy = 0*A.getData(1,0);
        double dvz = 0*A.getData(2,0);



        Xn.setData(0, 0, dx);
        Xn.setData(1, 0, dy);
        Xn.setData(2, 0, dz);

        Xn.setData(3, 0, dvx);
        Xn.setData(4, 0, dvy);
        Xn.setData(5, 0, dvz);

        return Xn;
    }

    private Matrix getOmega(double t) {
        Double key = new BigDecimal(t).setScale(1, RoundingMode.DOWN).doubleValue();
        if (map.containsKey(key))
            params = map.get(key);
        Omega = Rotate.Rz(omega,params[0]);
        Omega = Rotate.Ry(Omega,params[1]);
        return Omega;
    }
}
