package com.edu.sys.models;



import com.edu.sys.matrix.Matrix;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Nixy
 * Date: 20.09.13
 * Time: 7:33
 * To change this template use File | Settings | File Templates.
 */
public abstract class AbstractModel {
    protected List<Matrix> X;
    int size; // размерность фазового вектора
    public double step; // шаг интегрирования

    protected AbstractModel(Matrix initCondition, double step){
        X = new ArrayList<Matrix>();
        if (initCondition != null){
            X.add(initCondition);
            this.step = step;
            size = initCondition.getRow();
        }
    }

    public abstract boolean isEnough(Object ... params);
    public abstract Matrix F(double t,Matrix Xn);

    public  void setX(Matrix X){
        if (X!=null){
            this.X.add(X);
        }
    };

    public int getVectorSize(){
        return size;
    };

    public int getCount(){
        return X.size();
    }

    public Matrix getX(int i){
        if (i>-1 && i < X.size())
            return X.get(i);
        else return null;
    }

    public void recycle(){
        for(int i = 0 ; i < X.size() ; i++){
            X.get(i).recycle();
        }
        X.clear();
        System.gc();
    }
}
