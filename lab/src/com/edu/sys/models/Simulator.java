package com.edu.sys.models;

import com.edu.sys.matrix.Matrix;
import com.edu.sys.matrixoperations.*;
import com.edu.sys.quaternion.Quaternion;
import com.edu.sys.vectoroperations.Angle;
import com.edu.sys.vectoroperations.Module;
import com.edu.sys.vectoroperations.Norm;
import com.edu.sys.vectoroperations.Scalar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import static java.lang.Math.*;

/**
 * Created by Nixy on 03.05.14.
 */
public class Simulator extends AbstractModel {

    public static final int SELF_MOTION_SIZE = 12;
    public static final int TARGET_MOTION_SIZE = 6;

    public List<Matrix> Y = new ArrayList<Matrix>();

    public Matrix x1;
    public Matrix y1;
    public Matrix z1;

   SelfMotion selfMotion;

    TargetMotion targetMotion;

    public Simulator(Matrix initCondition, double step,HashMap<Double,double[]> map) {
        super(initCondition, step);
        Matrix selfInit = new Matrix(SELF_MOTION_SIZE,1);
        for (int i = 0 ; i < SELF_MOTION_SIZE ; i++)
            selfInit.setData(i,0,initCondition.getData(i,0));
        Matrix targetInit = new Matrix(TARGET_MOTION_SIZE,1);
        for (int i = 0 ; i < TARGET_MOTION_SIZE ; i++)
            targetInit.setData(i,0,initCondition.getData(SELF_MOTION_SIZE+i,0));

        selfMotion = new SelfMotion(selfInit,step);
        targetMotion = new TargetMotion(targetInit,step,map);
    }

    @Override
    public boolean isEnough(Object... params) {
        return false;
    }

    @Override
    public Matrix F(double t, Matrix Xn) {
        Matrix selfXn = new Matrix(SELF_MOTION_SIZE,1);
        for (int i = 0 ; i < SELF_MOTION_SIZE ; i++)
            selfXn.setData(i,0,Xn.getData(i,0));
        Matrix targetXn = new Matrix(TARGET_MOTION_SIZE,1);
        for (int i = 0 ; i < TARGET_MOTION_SIZE ; i++)
            targetXn.setData(i,0,Xn.getData(SELF_MOTION_SIZE+i,0));
   //    for (int i = 0 ; i < 3; i++)
    //        selfXn.setData(SELF_MOTION_SIZE+i-3,0,targetXn.getData(i,0));
        selfMotion.F(t,selfXn);
        targetMotion.F(t,targetXn);

        for (int i = 0 ; i < SELF_MOTION_SIZE ; i++)
            Xn.setData(i,0,selfXn.getData(i,0));

        for (int i = 0 ; i < TARGET_MOTION_SIZE ; i++)
            Xn.setData(SELF_MOTION_SIZE+i,0,targetXn.getData(i,0));

    return Xn;
}

    @Override
    public void setX(Matrix X) {
        super.setX(X);

        Matrix selfV = X.getVector(0,3,6,Matrix.TYPE_COL);
        Matrix selfX = X.getVector(0,0,3,Matrix.TYPE_COL);
        Matrix target = X.getVector(0,SELF_MOTION_SIZE,SELF_MOTION_SIZE+3,Matrix.TYPE_COL);
        Matrix D = Sub2.calc(target,selfX);
        double d = Module.calc(D);
        selfV = Norm.calc(selfV);
        D = Norm.calc(D);

        double alpha = selfV.getData(0,0) == 0 ? PI/2 : atan(selfV.getData(1,0)/ selfV.getData(0,0));
        double delta = abs(selfV.getData(0,0))+abs(selfV.getData(1,0)) == 0 ? PI/2 : atan(selfV.getData(2,0)/sqrt(pow(selfV.getData(0,0),2)+pow(selfV.getData(1,0),2)));

        delta  = ( selfV.getData(0,0) < 0 || selfV.getData(1,0) < 0 ? PI - delta: delta );
        delta = -delta;

         x1 = new Matrix(new double[][]{{1},{0},{0}});
         y1 = new Matrix(new double[][]{{0},{1},{0}});
         z1 = new Matrix(new double[][]{{0},{0},{1}});

        Quaternion q = new Quaternion(alpha,new Matrix(new double[][]{{0},{0},{1}}));
        Matrix Q = q.toMatrix();

        x1 = Multi.calc(Q,x1);
        y1 = Multi.calc(Q,y1);
        z1 = Multi.calc(Q, z1);

        q = new Quaternion(delta,y1);
        Q = q.toMatrix(); 
        x1 = Multi.calc(Q,x1);
        y1 = Multi.calc(Q,y1);
        z1 = Multi.calc(Q,z1);

        double f1 = Scalar.calc(x1,y1);
        double f2 = Scalar.calc(x1,z1);
        double f3 = Scalar.calc(y1,z1);


        double flag1 = Module.calc(Sub2.calc(x1,selfV));
        if (abs(flag1+f1+f2+f3) > 1e-15)
            flag1 = 0;

        double fiZ = Angle.calc(z1, D);
        double fiY = Angle.calc(y1,D);

        double omegaZ = Y.size() > 1 ? (fiZ - Y.get(Y.size()-1).getData(1,0))/step :  fiZ/step;
        double omegaY = Y.size() > 1 ? (fiY - Y.get(Y.size()-1).getData(2,0))/step :  fiY/step;

        Matrix y = new Matrix(new double[][]{{d},{fiZ},{fiY},{omegaZ},{omegaY}});



        Y.add(y);
    }
}
