package com.edu.sys.models;

import com.edu.sys.matrix.Matrix;
import com.edu.sys.matrixoperations.Rotate;
import com.edu.sys.vectoroperations.Module;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;

/**
 * Created by Nixy on 30.04.14.
 */
public class TargetMotion2 extends AbstractModel {

    public static final double MAX_A =  9999.81*1.5;
    private static final double T = 1;
    double[] params;
    HashMap<Double,double[]>  map ;

    public Matrix omega = new Matrix(new double[][]{{1},
            {0},
            {0}});
    Matrix Omega;
    public TargetMotion2(Matrix initCondition, double step, HashMap<Double, double[]> map) {
        super(initCondition, step);
        this.map = map;
    }

    @Override
    public boolean isEnough(Object... params) {
        return false;
    }

    @Override
    public Matrix F(double t, Matrix Xn) {
        double x = Xn.getData(0,0);
        double y = Xn.getData(1,0);
        double z = Xn.getData(2,0);

        double vx = Xn.getData(3,0);
        double vy = Xn.getData(4,0);
        double vz = Xn.getData(5,0);

        getNewParams(t);

        Matrix V = new Matrix(Xn.getVector(0,3,6,Matrix.TYPE_COL));
        V = Rotate.Rz(V,params[0]);
        V = Rotate.Ry(V,params[1]);



        Matrix A = new Matrix(new double[][]{{0},{0},{params[2]}});
        A = Rotate.Ry(A,-params[1]);
        A = Rotate.Rz(A,-params[0]);

        double ma = Module.calc(A);
        ma = ma > MAX_A ? MAX_A/ma : 1;
        A = A.getWithFactor(ma);

        double dx = vx;
        double dy = vy;
        double dz = vz;

        double dvx = A.getData(0,0);
        double dvy = A.getData(1,0);
        double dvz = A.getData(2,0);


        Xn.setData(0, 0, dx);
        Xn.setData(1, 0, dy);
        Xn.setData(2, 0, dz);

        Xn.setData(3, 0, dvx);
        Xn.setData(4, 0, dvy);
        Xn.setData(5, 0, dvz);

        return Xn;
    }

    private void getNewParams(double t) {
        Double key = new BigDecimal(t).setScale(1, RoundingMode.DOWN).doubleValue();
        if (map.containsKey(key))
            params = map.get(key);

    }
}
