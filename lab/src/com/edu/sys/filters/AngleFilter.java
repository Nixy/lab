package com.edu.sys.filters;

import com.edu.sys.matrix.Matrix;
import com.edu.sys.matrixoperations.Multi;
import com.edu.sys.models.Simulator;

/**
 * Created by Nixy on 14.05.14.
 */
public class AngleFilter extends LinearKlamanFilter {


    /*
    *   X = (fiY,fiZ,omegaY,omegaZ}
    *
    *   d fiY /dt = omegaY
    *   d fiZ /dt = omegaZ
    *   d omegaY /dt = 0
    *   d omegaZ /dt = 0
    *
    *   Y = {fiY,fiZ,omegaY,omegaZ)
    *
    * */

    Simulator simulator;
    double step;
    int t;

    public AngleFilter(Simulator simulator){
        this.simulator = simulator;
        step = simulator.step;
        getDispEta();
        getDispXi();
        getU();
        getH();
        getF();
        Xo = new Matrix(4,1);
        Ko = new Matrix(new double[][]{{10,0,step,0},
                                       {0,10,0,step},
                                       {step,0,10,0},
                                       {0,step,0,10}});
        XoList.add(Xo);
        KoList.add(Ko);
        for (t = 0 ; t < simulator.Y.size()-1; t++){
            getXf();
            getKf();
            getKo();
            getXo();
        }
    }

    @Override
    protected void getDispXi() {
        DispXi = new Matrix(4,4);
    }

    @Override
    protected void getDispEta() {
        DispEta = Matrix.E(4);
    }

    @Override
    protected void getU() {
        U = new Matrix(4,1);
    }

    @Override
    protected void getF() {
        F = new Matrix(new double[][]{{1,0,step,0},
                                      {0,1,0,step},
                                      {0,0,1,0},
                                      {0,0,0,1}});
    }

    @Override
    protected void getH() {
        H = new Matrix(new double[][]{{1,0,step,0},
                                      {0,1,0,step},
                                      {0,0,1,0},
                                      {0,0,0,1}});
    }

    @Override
    protected Matrix getY() {
        Matrix Y = simulator.Y.get(t);
        Matrix Angle = Y.getVector(0,1,5,Matrix.TYPE_COL);
        return Angle;
    }

    @Override
    protected Matrix getG() {
        return Multi.calc(H,Xf);
    }


}
