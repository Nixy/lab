package com.edu.sys.matrix;

/**
 * Created with IntelliJ IDEA.
 * User: Nixy
 * Date: 20.09.13
 * Time: 7:40
 * To change this template use File | Settings | File Templates.
 */
public class Matrix {
    double data[][];
    int row = -1;
    int col = -1;

    public static final int TYPE_COL = 0;
    public static final int TYPE_ROW = 1;

    public int getCol() {
        return col;
    }

    private void setCol(int col) {
        this.col = col;
    }

    public int getRow() {
        return row;
    }

    private void setRow(int row) {
        this.row = row;
    }

    public Matrix(){
        // default constructor;
    }

    public Matrix(int row,int col){
        if (row > 0 && col >0){
            setCol(col);
            setRow(row);
            initData();
        }

    }

    public Matrix getVector(int n,int start,int end,int type){
        switch (type){
            case TYPE_COL:
                if (n > -1 && n < col && start > -1 && start <= end && end <= row){
                    Matrix result = new Matrix(end-start,1);
                    for (int i = 0, j = start; j < end; i++,j++){
                        result.setData(i,0,getData(j,n));
                    }
                    return result;
                }
                return null;
            case TYPE_ROW:
                if (n > -1 && n < row && start > -1 && start <= end && end <= col){
                    Matrix result = new Matrix(1,end-start);
                    for (int i = 0, j = start; j < end; i++,j++){
                        result.setData(0,i,getData(n,j));
                    }
                    return result;
                }
                return null;
        }
        return null;
    }

    public Matrix(Double... args){
        setCol(1);
        setRow(args.length);
        initData();
        for (int i=0; i<getRow();i++){
            setData(i,0,args[i]);
        }
    }

    public Matrix(Matrix A){
        setCol(A.getCol());
        setRow(A.getRow());
        initData();
        for (int i = 0 ; i < row ; i++)
            for (int j = 0 ; j < col ; j++)
                setData(i,j,A.getData(i,j));
    }

    public Matrix(double[][] array){
        if (array != null)
            if (array[0].length > 0){
                row = array.length;
                col = array[0].length;
                initData();
                for (int i = 0 ; i < row ; i++)
                    for (int j = 0 ; j < col ; j++)
                    data[i][j] = array[i][j];
            }
    }


    public double getData(int row,int col){
        if (row > -1 && row < getRow())
            if(col > -1 && col < getCol()){
                return data[row][col];
            }
        return Double.NaN;
    }

    public void setData(int row,int col,double data){
        if (row > -1 && row < getRow())
            if(col > -1 && col < getCol()){
                this.data[row][col] = data;
            }
    }

    public Matrix copy(Matrix A){
        if (!isSameSize(A)){
            recycle();
            setCol(A.getCol());
            setRow(A.getRow());
            initData();
        }
        for (int i = 0 ; i < row ; i++)
            for (int j = 0 ; j < col ; j++)
                setData(i,j,A.getData(i,j));
        return this;
    }

    public Matrix copyWithFactor(Matrix A,double factor){
        if (!isSameSize(A)){
            recycle();
            setCol(A.getCol());
            setRow(A.getRow());
            initData();
        }
        for (int i = 0 ; i < row ; i++)
            for (int j = 0 ; j < col ; j++)
                setData(i,j,A.getData(i,j)*factor);
        return this;
    }

    private void initData(){
        data = new double[getRow()][getCol()];
    }

    public void recycle(){
        for (int i = 0 ; i < row ; i++)
            data[i] = null;
        data = null;
        row = -1;
        col = -1;
    }

    public boolean isSameSize(Matrix A){
        return A.getCol() == getCol() && A.getRow() == getRow();
    }

    @Override
    public String toString() {
        String m = "{";
        for (int i = 0; i < getRow(); i++){
            m += "{";
            for (int j = 0 ; j < getCol(); j++){
                m += String.valueOf(data[i][j]);
                if (j < getCol()-1){
                    m+= ",";
                }
            }
            m += "}";
            if (i < getRow()-1){
                m += ",";
            }
        }
        m += "}";
        return m;
    }

    public static Matrix E(int n){
        Matrix e = new Matrix(n,n);
        for (int i = 0; i < n; i++) e.setData(i,i,1);
        return e;
    }

    public Matrix getWithFactor(double c){
        Matrix A = new Matrix().copy(this);
        for (int i = 0 ; i < row ; i++)
            for (int j = 0 ; j < col ; j++)
                A.setData(i,j,A.getData(i,j)*c);
        return A;
    }
}
