package com.edu.sys.vectoroperations;

import com.edu.sys.matrix.Matrix;

/**
 * Created by Nixy on 11.05.14.
 */
public class Angle {
    private static boolean isVector(Matrix m){
        if ((m.getRow() > 1 && m.getCol() == 1)) return true; else  return false;
    }

    public static double calc(Matrix... args){
        if (args.length > 0 && isVector(args[0]) && isVector(args[1]) ){
            double m1 = Module.calc(args[0]);
            double m2 = Module.calc(args[1]);
            double s = Scalar.calc(args);
            if (m1*m2 == 0) return Double.NaN;
            return Math.acos(s/m1/m2);
        }
        return  Double.NaN;
    }
}
