package com.edu.sys.functions;


import com.edu.sys.matrix.Matrix;

/**
 * Created with IntelliJ IDEA.
 * User: Миша
 * Date: 08.10.13
 * Time: 21:11
 * To change this template use File | Settings | File Templates.
 */
public class Diff {

    public static double calc(Function f,double h,int i, Matrix... m){
        double yn = f.calc(m);
        double tmp = m[0].getData(i,0);
        m[0].setData(i,0,tmp+h);
        double yn1 = f.calc(m);
        m[0].setData(i,0,tmp);
        double dy = (yn1 - yn);
        return dy/h;
    }
}
