package com.edu.gui;

import com.edu.sys.filters.AngleFilter;
import com.edu.sys.filters.RangeFilter;
import com.edu.sys.integrators.RungeKutt;
import com.edu.sys.matrix.Matrix;
import com.edu.sys.models.Simulator;
import com.orsoncharts.Chart3D;
import com.orsoncharts.Chart3DFactory;
import com.orsoncharts.ChartPanel3D;
import com.orsoncharts.data.xyz.XYZDataset;
import com.orsoncharts.data.xyz.XYZSeries;
import com.orsoncharts.data.xyz.XYZSeriesCollection;
import com.orsoncharts.graphics3d.ViewPoint3D;
import com.orsoncharts.graphics3d.swing.DisplayPanel3D;
import com.orsoncharts.plot.XYZPlot;
import com.orsoncharts.renderer.xyz.ScatterXYZRenderer;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.IntervalXYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;
import static java.lang.Math.*;
import javax.swing.*;
import java.awt.*;

import java.util.*;

public class Main extends ApplicationFrame {

    public static final double STEP = 0.05;
    private static final int TYPE_MEASURE = 0;
    private static final int TYPE_MOTION = 1;
    private static final int TYPE_RATING_ANGLE = 2;
    private static final int TYPE_RATING_RANGE = 3;
    private static final int TYPE_ANGLE_ERR = 4;
    private static final int TYPE_RANGE_ERR = 5;
    static Simulator simulator;
    static AngleFilter filter1;
    static RangeFilter filter2;

    public static final Dimension DEFAULT_CONTENT_SIZE
            = new Dimension(760, 480);

    public Main(String title) {
        super(title);
        JTabbedPane tab = new JTabbedPane();
        JPanel chartsPanel1 = new JPanel(new GridLayout(2,2));

        chartsPanel1.add(getXYPanel("X", "Y", "Self XY", "XY", 0, 1));
        chartsPanel1.add(getXYPanel("Y", "Z", "Self YZ", "YZ", 1, 2));
        chartsPanel1.add(getXYPanel("X", "Z", "Self XZ", "XZ", 0, 2));

        tab.add("Собственное движение",chartsPanel1);

        JPanel chartsPanel2 = new JPanel(new GridLayout(2,2));

        chartsPanel2.add(getXYPanel("X", "Y", "Target XY", "XY", Simulator.SELF_MOTION_SIZE, Simulator.SELF_MOTION_SIZE+ 1));
        chartsPanel2.add(getXYPanel("Y", "Z", "Target YZ", "YZ", Simulator.SELF_MOTION_SIZE+1, Simulator.SELF_MOTION_SIZE+2));
        chartsPanel2.add(getXYPanel("X","Z","Target XZ","XZ",Simulator.SELF_MOTION_SIZE,Simulator.SELF_MOTION_SIZE+2));

        tab.add("Движение цели",chartsPanel2);

        JPanel chartsPanel3 = new JPanel(new GridLayout(2,2));

        chartsPanel3.add(getChartT("D","Дальность","D",0,TYPE_MEASURE));
        chartsPanel3.add(getChartT("phiZ","Бортовой угол прицеливания","phiZ",1,TYPE_MEASURE));
        chartsPanel3.add(getChartT("phiY","Угол места","phiY",2,TYPE_MEASURE));
        chartsPanel3.add(getChartT("OmegaZ","Угловая скорость phiZ","OmegaZ",3,TYPE_MEASURE));
        chartsPanel3.add(getChartT("OmegaY","Угловая скорость phiY","OmegaY",4,TYPE_MEASURE));

        tab.add("Измерения",chartsPanel3);

        tab.add("3D график",getXYZPanel());

        JPanel chartsPanel4 = new JPanel(new GridLayout(2,2));
        chartsPanel4.add(getChartT("phiZ","Бортовой угол прицеливания","Оценка phiZ",0,TYPE_RATING_ANGLE));
        chartsPanel4.add(getChartT("phiY","Угол места","Оценка phiY",1,TYPE_RATING_ANGLE));
        chartsPanel4.add(getChartT("OmegaZ","Угловая скорость phiZ","Оценка OmegaZ",2,TYPE_RATING_ANGLE));
        chartsPanel4.add(getChartT("OmegaY","Угловая скорость phiY","Оценка OmegaY",3,TYPE_RATING_ANGLE));

        tab.add("Оценки углов",chartsPanel4);

        JPanel chartsPanel5 = new JPanel(new GridLayout(2,2));
        chartsPanel5.add(getChartT("phiZ","Бортовой угол прицеливания","Ошибка phiZ",0,TYPE_ANGLE_ERR));
        chartsPanel5.add(getChartT("phiY","Угол места","Ошибка phiY",1,TYPE_ANGLE_ERR));
        chartsPanel5.add(getChartT("OmegaZ","Угловая скорость phiZ","Ошибка OmegaZ",2,TYPE_ANGLE_ERR));
        chartsPanel5.add(getChartT("OmegaY","Угловая скорость phiY","Ошибка OmegaY",3,TYPE_ANGLE_ERR));

        tab.add("Ошибки оценки углов",chartsPanel5);

        JPanel chartsPanel6 = new JPanel(new GridLayout(1,1));
        chartsPanel6.add(getChartT("D","Дальность","Оценка D",0,TYPE_RATING_RANGE));

        tab.add("Оценка дальности",chartsPanel6);

        JPanel chartsPanel7 = new JPanel(new GridLayout(1,1));
        chartsPanel7.add(getChartT("D","Дальность","Ошибка D",0,TYPE_RANGE_ERR));

        tab.add("Ошибка оценки дальности",chartsPanel7);

        setContentPane(tab);
    }

    private static JPanel getChartT(String y,String title,String series,int j,int type){

        IntervalXYDataset data1 = createDatasetT(series,j,type);


        final XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
        renderer.setBaseShapesVisible(false);


        XYPlot plot1 = new XYPlot(data1, new NumberAxis("t"), new NumberAxis(y), renderer);
        plot1.setBackgroundPaint(Color.lightGray);
        plot1.setDomainGridlinePaint(Color.white);
        plot1.setRangeGridlinePaint(Color.white);

        boolean legend = data1.getSeriesCount() > 1 ;
        JFreeChart chart1 = new JFreeChart(title,
                JFreeChart.DEFAULT_TITLE_FONT, plot1, legend);
        chart1.setBackgroundPaint(Color.white);
        ChartPanel panel1 = new ChartPanel(chart1);
        JPanel content = new JPanel(new BorderLayout());
        content.setPreferredSize(DEFAULT_CONTENT_SIZE);
        content.add(panel1);
        return content;
    }

    private static IntervalXYDataset createDatasetT(String series,int y,int type) {
        // create dataset 1...
        XYSeriesCollection collection = new XYSeriesCollection();
        collection.addSeries(new XYSeries(series));

        switch (type){
            case TYPE_RATING_ANGLE:
                collection.addSeries(new XYSeries("Истинное значение"));
                break;
            case TYPE_RATING_RANGE:
                collection.addSeries(new XYSeries("Истинное значение"));
                break;
            case TYPE_ANGLE_ERR:
                collection.addSeries(new XYSeries("+3 sigma"));
                collection.addSeries(new XYSeries("-3 sigma"));
                break;
            case TYPE_RANGE_ERR:
                collection.addSeries(new XYSeries("+3 sigma"));
                collection.addSeries(new XYSeries("-3 sigma"));
                break;
        }

        for (int i = 0 ; i < simulator.getCount() ; i++){
            switch (type){
                case TYPE_MEASURE :
                    if (i < simulator.Y.size())
                        collection.getSeries(0).add(i * simulator.step, simulator.Y.get(i).getData(y, 0));
                    break;
                case TYPE_MOTION :
                    collection.getSeries(0).add(i * simulator.step, simulator.getX(i).getData(y, 0));
                    break;
                case TYPE_RATING_ANGLE :
                    if (i < simulator.Y.size()){
                        collection.getSeries(0).add(i * simulator.step, filter1.XoList.get(i).getData(y, 0));
                        collection.getSeries(1).add(i * simulator.step, simulator.Y.get(i).getData(y + 1, 0));
                    }
                    break;
                case TYPE_ANGLE_ERR:
                    if (i < simulator.Y.size()){
                        collection.getSeries(0).add(i * simulator.step, filter1.XoList.get(i).getData(y, 0)-simulator.Y.get(i).getData(y+1,0));
                        double sigma =   sqrt(filter1.KoList.get(i).getData(y, y));
                        collection.getSeries(1).add(i * simulator.step,3*sigma);
                        collection.getSeries(2).add(i * simulator.step,-3*sigma);
                    }
                    break;
                case TYPE_RATING_RANGE:
                    if (i < simulator.Y.size()){
                        double d = 0;
                        double di = simulator.Y.get(i).getData(0, 0);
                      // double t = i * simulator.step;
                        double t =  simulator.step;
                        for(int j = 0 ; j < filter2.XoList.get(i).getRow(); j++){
                            d += filter2.XoList.get(i).getData(j,0)*pow(t,j)/fact(j);
                        }
                        t = i * simulator.step;
                        collection.getSeries(0).add(t, d);
                        collection.getSeries(1).add(t, di);
                    }
                    break;
                case TYPE_RANGE_ERR:
                    if (i < simulator.Y.size()){
                        double d = 0;
                      //  double t = i * simulator.step;
                        double t =  simulator.step;

                        for(int j = 0 ; j < filter2.XoList.get(i).getRow(); j++){
                            d += filter2.XoList.get(i).getData(j,0)*pow(t,j)/fact(j);
                        }
                        t = i * simulator.step;
                        collection.getSeries(0).add(t,simulator.Y.get(i).getData(0, 0)- d);
                        double sigma =   filter2.getSigma(i);
                        collection.getSeries(1).add(i * simulator.step,3*sigma);
                        collection.getSeries(2).add(i * simulator.step,-3*sigma);
                    }
                    break;
            }

        }

        return collection;
    }

    static double fact(int n){
        if (n == 0) return 1;
        return n * fact(n-1);
    }
    
    private static JPanel getXYZPanel(){
        XYZDataset data3 = createDatasetXYZ();
        Chart3D chart = Chart3DFactory.createScatterChart("TargetMotion",
                "", data3, "X", "Y", "Z");
        XYZPlot plot3 = (XYZPlot) chart.getPlot();
        ScatterXYZRenderer renderer = (ScatterXYZRenderer) plot3.getRenderer();
        renderer.setSize(0.1);

        chart.setViewPoint(ViewPoint3D.createAboveLeftViewPoint(40));
        ChartPanel3D chartPanel = new ChartPanel3D(chart);
        chartPanel.zoomToFit(DEFAULT_CONTENT_SIZE);
        JPanel content = new JPanel(new BorderLayout());
        content.setPreferredSize(DEFAULT_CONTENT_SIZE);
        content.add(new DisplayPanel3D(chartPanel));
        return content;
    }

    private static JPanel getXYPanel(String x,String y,String title,String series,int i,int j){
        IntervalXYDataset data1 = createDatasetXY(series,i,j);

        final XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
        Shape shape = new Rectangle(1,1);
        renderer.setBaseShape(shape);
        renderer.setSeriesLinesVisible(0, false);


        XYPlot plot1 = new XYPlot(data1, new NumberAxis(x), new NumberAxis(y), renderer);
        plot1.setBackgroundPaint(Color.lightGray);
        plot1.setDomainGridlinePaint(Color.white);
        plot1.setRangeGridlinePaint(Color.white);


        JFreeChart chart1 = new JFreeChart(title,
                JFreeChart.DEFAULT_TITLE_FONT, plot1, false);
        chart1.setBackgroundPaint(Color.white);
        ChartPanel panel1 = new ChartPanel(chart1);
        JPanel content = new JPanel(new BorderLayout());
        content.setPreferredSize(DEFAULT_CONTENT_SIZE);
        content.add(panel1);
        return content;
    }



    private static IntervalXYDataset createDatasetXY(String series,int x,int y) {
        // create dataset 1...
        XYSeries series1 = new XYSeries(series);
        for (int i = 0 ; i < simulator.getCount() ; i++){
            series1.add(simulator.getX(i).getData(x,0),simulator.getX(i).getData(y,0));
        }
        return new XYSeriesCollection(series1);

    }

    private static XYZDataset createDatasetXYZ() {

        XYZSeries self = new XYZSeries("self");
        XYZSeries target = new XYZSeries("target");
        XYZSeries x1 = new XYZSeries("x1");
        XYZSeries y1 = new XYZSeries("y1");
        XYZSeries z1 = new XYZSeries("z1");



        for (int i = 0 ;  i < simulator.getCount(); i+=2){
            self.add(simulator.getX(i).getData(0,0),simulator.getX(i).getData(1,0),simulator.getX(i).getData(2,0));
            target.add(simulator.getX(i).getData(Simulator.SELF_MOTION_SIZE+0,0),
                    simulator.getX(i).getData(Simulator.SELF_MOTION_SIZE+1,0),
                    simulator.getX(i).getData(Simulator.SELF_MOTION_SIZE+2,0));
        }
        for (int i = 1 ; i < 500 ; i+=10){
            int n = 1;
            x1.add(simulator.getX(simulator.getCount()-1).getData(0,0)*n+simulator.x1.getData(0,0)*i,
                    simulator.getX(simulator.getCount()-1).getData(1,0)*n+simulator.x1.getData(1,0)*i,
                    simulator.getX(simulator.getCount()-1).getData(2,0)*n+simulator.x1.getData(2,0)*i);

            y1.add(simulator.getX(simulator.getCount()-1).getData(0,0)*n+simulator.y1.getData(0,0)*i,
                    simulator.getX(simulator.getCount()-1).getData(1,0)*n+simulator.y1.getData(1,0)*i,
                    simulator.getX(simulator.getCount()-1).getData(2,0)*n+simulator.y1.getData(2,0)*i);

            z1.add(simulator.getX(simulator.getCount()-1).getData(0,0)*n+simulator.z1.getData(0,0)*i,
                    simulator.getX(simulator.getCount()-1).getData(1,0)*n+simulator.z1.getData(1,0)*i,
                    simulator.getX(simulator.getCount()-1).getData(2,0)*n+simulator.z1.getData(2,0)*i);
        }

        XYZSeriesCollection dataset = new XYZSeriesCollection();
        dataset.add(target);
        dataset.add(self);
        dataset.add(x1);
        dataset.add(y1);
        dataset.add(z1);



        return dataset;
    }




    public static void main(String[] args) {
	// write your code here
        Matrix init = new Matrix(Simulator.SELF_MOTION_SIZE+Simulator.TARGET_MOTION_SIZE,1);

        init.setData(Simulator.SELF_MOTION_SIZE,0,0);         // Target X
        init.setData(Simulator.SELF_MOTION_SIZE+1,0,0);       // Target Y
        init.setData(Simulator.SELF_MOTION_SIZE+2,0,4000);    // Target Z
        init.setData(Simulator.SELF_MOTION_SIZE+3,0,100);        // Target VX
        init.setData(Simulator.SELF_MOTION_SIZE+4,0,0);       // Target VY
        init.setData(Simulator.SELF_MOTION_SIZE+5,0,0);    // Target VZ


        init.setData(0,0,-1./sqrt(28)*4000);    //  Self X
        init.setData(1,0,-1./sqrt(82)*4000);    // Self Y
        init.setData(2,0,0);    // Self  Z
        init.setData(Simulator.SELF_MOTION_SIZE-3,0,0);    //  Xc
        init.setData(Simulator.SELF_MOTION_SIZE-2,0,0);    //  Yc
        init.setData(Simulator.SELF_MOTION_SIZE-1,0,4000);    //  Zc


        HashMap<Double,double[]> map = new HashMap<Double, double[]>();
        fillMap(map);
        simulator = new Simulator(init,STEP,map);

        RungeKutt integrator = new RungeKutt(simulator,0,79,STEP);
        integrator.integrate();

        filter1 = new AngleFilter(simulator);
        filter2 = new RangeFilter(simulator);

        Main demo = new Main(
                "lab");
        demo.pack();
        RefineryUtilities.centerFrameOnScreen(demo);
        demo.setVisible(true);


    }

    private static void fillMap(HashMap<Double, double[]> map) {
        map.put(0.,new double[]{toRadians(50),toRadians(0),1000});
       map.put(20.,new double[]{toRadians(10),toRadians(50),10000});
        map.put(50.,new double[]{toRadians(-50),toRadians(0),30000});
    }
}

