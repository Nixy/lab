package com.edu.gui;

import com.edu.sys.filters.AngleFilter;
import com.edu.sys.filters.RangeFilter;
import com.edu.sys.integrators.RungeKutt;
import com.edu.sys.matrix.Matrix;
import com.edu.sys.matrixoperations.Add2;
import com.edu.sys.matrixoperations.AddWhole;
import com.edu.sys.matrixoperations.Sub2;
import com.edu.sys.models.Simulator;
import com.orsoncharts.Chart3D;
import com.orsoncharts.Chart3DFactory;
import com.orsoncharts.ChartPanel3D;
import com.orsoncharts.data.xyz.XYZDataset;
import com.orsoncharts.data.xyz.XYZSeries;
import com.orsoncharts.data.xyz.XYZSeriesCollection;
import com.orsoncharts.graphics3d.ViewPoint3D;
import com.orsoncharts.graphics3d.swing.DisplayPanel3D;
import com.orsoncharts.plot.XYZPlot;
import com.orsoncharts.renderer.xyz.ScatterXYZRenderer;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.IntervalXYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import static java.lang.Math.*;

public class Main extends ApplicationFrame {

    public static final double STEP = 0.05;
    private static final int TYPE_MEASURE = 0;
    private static final int TYPE_MOTION = 1;
    private static final int TYPE_RATING_ANGLE = 2;
    private static final int TYPE_RATING_RANGE = 3;
    private static final int TYPE_ANGLE_ERR = 4;
    private static final int TYPE_RANGE_ERR = 5;
    private static final int TYPE_RATING_POS = 6;
    private static final int TYPE_RATING_VEL = 7;

    private static final int PROP_TARGET_X = 0;
    private static final int PROP_TARGET_Y = 1;
    private static final int PROP_TARGET_Z = 2;

    private static final int PROP_SELF_X = 3;
    private static final int PROP_SELF_Y = 4;
    private static final int PROP_SELF_Z = 5;

    private static final int PROP_TARGET_TIME = 6;
    private static final int PROP_TARGET_ANGLE_1 = 7;
    private static final int PROP_TARGET_ANGLE_2 = 8;
    private static final int PROP_TARGET_RADIUS = 9;

    private static final int PROP_INTEGRATOR_STEP = 10;
    private static final int PROP_INTEGRATOR_END = 11;

    static Simulator simulator;
    static AngleFilter filter1;
    static RangeFilter filter2;

    static List<Matrix> ratPos = new ArrayList<Matrix>();
    static List<Matrix> ratVel = new ArrayList<Matrix>();
    static HashMap<Double,double[]> map = new HashMap<Double, double[]>();
    List<Double> mapKeys = new ArrayList<Double>();
    {
       mapKeys.add(0.);
       mapKeys.add(50.);
    }
    static{
        map.put(0.,new double[]{toRadians(0),toRadians(0),1000});
        map.put(50.,new double[]{toRadians(0),toRadians(90),1000});
        //  map.put(20.,new double[]{toRadians(-50),toRadians(0),30000});
    }

    public static final Dimension DEFAULT_CONTENT_SIZE
            = new Dimension(760, 480);

    /*
    *
    * ИНИЦИАЛИЗАЦИЯ
    *
    * */
    public static void main(String[] args) {


        Main demo = new Main(
                "lab");
        demo.pack();
        RefineryUtilities.centerFrameOnScreen(demo);
        demo.setVisible(true);


    }

    private static void init(double... params){
        Matrix init = new Matrix(Simulator.SELF_MOTION_SIZE+Simulator.TARGET_MOTION_SIZE,1);

        init.setData(Simulator.SELF_MOTION_SIZE,0,0);         // Target X
        init.setData(Simulator.SELF_MOTION_SIZE+1,0,0);       // Target Y
        init.setData(Simulator.SELF_MOTION_SIZE+2,0,7000);    // Target Z
        init.setData(Simulator.SELF_MOTION_SIZE+3,0,0);       // Target teta



        init.setData(0,0,0);    //  Self X
        init.setData(1,0,0);    // Self Y
        init.setData(2,0,0);    // Self  Z
        init.setData(Simulator.SELF_MOTION_SIZE-3,0,0);    //  Xc
        init.setData(Simulator.SELF_MOTION_SIZE-2,0,0);    //  Yc
        init.setData(Simulator.SELF_MOTION_SIZE-1,0,7000);    //  Zc

        simulator = new Simulator(init,STEP,map);

        RungeKutt integrator = new RungeKutt(simulator,0,params[0],STEP);
        integrator.integrate();

        filter1 = new AngleFilter(simulator);
        filter2 = new RangeFilter(simulator);
        for (int i = 0,j = 1 ; i < simulator.getCount() && j < simulator.Y.size() ; i++,j++){ // ПЕРЕСЧЕТ ОЦЕНОК УГЛОВ В КООРДИНАТЫ

            double cosX = cos(filter1.XoList.get(j).getData(0, 0));
            double cosY = cos(filter1.XoList.get(j).getData(1, 0));
            double cosZ = cos(filter1.XoList.get(j).getData(2, 0));

            double mD = filter2.XoList.get(j).getData(0,0);
            double[] xyz = new double[]{cosX*mD,cosY*mD,cosZ*mD};
            Matrix x1 = simulator.S.get(i*4+1);
            Matrix y1 = simulator.S.get(i*4+2);
            Matrix z1 = simulator.S.get(i*4+3);
            Matrix D = AddWhole.calc(x1.getWithFactor(xyz[0]),y1.getWithFactor(xyz[1]),z1.getWithFactor(xyz[2]));
            ratPos.add(Add2.calc(simulator.getX(i).getVector(0,0,3,Matrix.TYPE_COL),D));

            double dCosX = -sin(filter1.XoList.get(j).getData(0, 0))*filter1.XoList.get(j).getData(3, 0);
            double dCosY = -sin(filter1.XoList.get(j).getData(1, 0))*filter1.XoList.get(j).getData(4, 0);
            double dCosZ = -sin(filter1.XoList.get(j).getData(2, 0))*filter1.XoList.get(j).getData(5, 0);

            double dD = filter2.XoList.get(j).getData(1,0);
            double[] dxyz = new double[]{cosX*dD + dCosX*mD,cosY*dD + dCosY*mD,cosZ*dD+dCosZ*mD};
            D = AddWhole.calc(x1.getWithFactor(dxyz[0]),y1.getWithFactor(dxyz[1]),z1.getWithFactor(dxyz[2]));
            Matrix V = Sub2.calc(simulator.getX(j).getVector(0,0,3,Matrix.TYPE_COL),simulator.getX(i).getVector(0,0,3,Matrix.TYPE_COL));
            V = V.getWithFactor(1./simulator.step);
            ratVel.add(Add2.calc(V,D));
        }
    }

    JTabbedPane tab;
    JTextArea endTime;

    HashMap<Integer,JTextField> properties = new HashMap<Integer, JTextField>();
    private JPanel getTextWithLabel(int id, String labelText, String editText){
        JTextField textField = new JTextField(editText,15);
        textField.setBorder(BorderFactory.createTitledBorder(labelText));
        JPanel panel = new JPanel(new BorderLayout());
        panel.add(textField,BorderLayout.CENTER);
        properties.put(id,textField);
        return panel;
    }

    String[] columnNames = {
            "Id",
            "Время[сек]",
            "Альфа[град.]",
            "Бета[град.]",
            "Радиус[м]"
    };

    private JTable getManeuverTable(String[] columnNames){

        Set<Double> set = map.keySet();
        Object[] keys = set.toArray();
        for (int i = 0; i < keys.length; i++){
            String[] row = new String[columnNames.length];
            row[0] = String.valueOf(i);
            double[] d = map.get(keys[i]);
            row[1] = String.valueOf(keys[i]);
            row[2] = String.valueOf(d[0]);
            row[3] = String.valueOf(d[1]);
            row[4] = String.valueOf(d[2]);

            model.addRow(row);
        }
        JTable table = new JTable();
        table.setBorder(new EmptyBorder(20, 20, 20, 20));
        table.setModel(model);
        return table;
    }

    DefaultTableModel model = new DefaultTableModel(columnNames,0);
    public Main(String title) {
        super(title);
        setExtendedState(Frame.MAXIMIZED_BOTH);
        tab = new JTabbedPane();

        JPanel panelTarget = new JPanel();
        JPanel panelSelf = new JPanel();
        panelTarget.setBorder(BorderFactory.createTitledBorder("Параметры цели"));
        panelSelf.setBorder(BorderFactory.createTitledBorder("Собственные параметры"));

        //Panel Target
        panelTarget.add(getTextWithLabel(PROP_TARGET_X, "Положение по X[м]:","0"));
        panelTarget.add(getTextWithLabel(PROP_TARGET_Y, "Положение по Y[м]:","0"));
        panelTarget.add(getTextWithLabel(PROP_TARGET_Z, "Положение по Z[м]:","3000"));
        JPanel panelManeuver = new JPanel();
        panelManeuver.setLayout(new BoxLayout(panelManeuver, BoxLayout.Y_AXIS));
        JPanel panelManeuverProperties = new JPanel(new GridLayout(2,2));
        panelManeuverProperties.setBorder(BorderFactory.createTitledBorder("Параметры маневров"));
        panelManeuverProperties.add(getTextWithLabel(PROP_TARGET_TIME, "Время[сек]", "0"));
        panelManeuverProperties.add(getTextWithLabel(PROP_TARGET_ANGLE_1, "Угол альфа[град.]", "0"));
        panelManeuverProperties.add(getTextWithLabel(PROP_TARGET_ANGLE_2, "Угол бета[град.]", "0"));
        panelManeuverProperties.add(getTextWithLabel(PROP_TARGET_RADIUS, "Радиус[м]","0"));
        panelManeuver.add(panelManeuverProperties);
        panelTarget.add(panelManeuver);
        JButton buttonAddToPath = new JButton("Добавить в маршрут") ;
        buttonAddToPath.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                double key =  Double.valueOf(properties.get(PROP_TARGET_TIME).getText());
                if (!map.containsKey(key) && key > mapKeys.get(mapKeys.size()-1)){
                    map.put(Double.valueOf(properties.get(PROP_TARGET_TIME).getText()),new double[]{
                            Double.valueOf(properties.get(PROP_TARGET_ANGLE_1).getText()),
                            Double.valueOf(properties.get(PROP_TARGET_ANGLE_2).getText()),
                            Double.valueOf(properties.get(PROP_TARGET_RADIUS).getText())
                    });
                    mapKeys.add(Double.valueOf(properties.get(PROP_TARGET_TIME).getText()));
                    model.addRow(new Object[]{mapKeys.size()-1,
                            properties.get(PROP_TARGET_TIME).getText(),
                            properties.get(PROP_TARGET_ANGLE_1).getText(),
                            properties.get(PROP_TARGET_ANGLE_2).getText(),
                            properties.get(PROP_TARGET_RADIUS).getText()

                    });
                }
            }
        });

        panelTarget.add(buttonAddToPath);
        JButton removeLast = new JButton("Удалить последнее");
        removeLast.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.removeRow(model.getRowCount()-1);
                map.remove(mapKeys.get(mapKeys.size()-1));
                mapKeys.remove(mapKeys.size()-1);
            }
        });
        panelTarget.add(removeLast);
        JScrollPane scrollPane = new JScrollPane(getManeuverTable(columnNames));
        panelTarget.add(scrollPane);


        //Panel Self
        panelSelf.add(getTextWithLabel(PROP_SELF_X, "Положение по X[м]:","0"));
        panelSelf.add(getTextWithLabel(PROP_SELF_Y, "Положение по Y[м]:","0"));
        panelSelf.add(getTextWithLabel(PROP_SELF_Z, "Положение по Z[м]:","0"));
        JPanel panelIntegrator = new JPanel();
        panelIntegrator.setBorder(BorderFactory.createTitledBorder("Параметры интегратора"));
        panelIntegrator.add(getTextWithLabel(PROP_INTEGRATOR_STEP, "Шаг интегрирования[сек]:", "0"));
        panelIntegrator.add(getTextWithLabel(PROP_INTEGRATOR_END, "Конечное время[сек]:", "0"));
        panelSelf.add(panelIntegrator);

        GridLayout experimentLayout = new GridLayout(1,3);
        JPanel panel0 = new JPanel();
        panel0.setLayout(experimentLayout);
        JButton startButton = new JButton("ПУСК");
        startButton.setFont(new Font("Arial", Font.BOLD, 40));
        endTime = new JTextArea("100");
        endTime.setBorder(BorderFactory.createTitledBorder("Конец интегрирования"));

        panel0.add(panelTarget);
        panel0.add(panelSelf);
        panel0.add(startButton);

        tab.add("Инициализация",panel0);
        setContentPane(tab);
        setPreferredSize(DEFAULT_CONTENT_SIZE);

        startButton.addActionListener( new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                init(Double.valueOf(endTime.getText()));
                JPanel chartsPanel1 = new JPanel(new GridLayout(2,2));

                chartsPanel1.add(getXYPanel("X", "Y", "Self XY", "XY", 0, 1));
                chartsPanel1.add(getXYPanel("Y", "Z", "Self YZ", "YZ", 1, 2));
                chartsPanel1.add(getXYPanel("X", "Z", "Self XZ", "XZ", 0, 2));

                tab.add("Собственное движение",chartsPanel1);

                JPanel chartsPanel2 = new JPanel(new GridLayout(2,2));

                chartsPanel2.add(getXYPanel("X", "Y", "Target XY", "XY", Simulator.SELF_MOTION_SIZE, Simulator.SELF_MOTION_SIZE+ 1));
                chartsPanel2.add(getXYPanel("Y", "Z", "Target YZ", "YZ", Simulator.SELF_MOTION_SIZE+1, Simulator.SELF_MOTION_SIZE+2));
                chartsPanel2.add(getXYPanel("X","Z","Target XZ","XZ",Simulator.SELF_MOTION_SIZE,Simulator.SELF_MOTION_SIZE+2));

                tab.add("Движение цели",chartsPanel2);

                JPanel chartsPanel3 = new JPanel(new GridLayout(2,2));

                chartsPanel3.add(getChartT("D","Дальность","D",0,TYPE_MEASURE));
                chartsPanel3.add(getChartT("phiX","Угол курса","phiX",1,TYPE_MEASURE));
                chartsPanel3.add(getChartT("phiY","Угол места","phiY",2,TYPE_MEASURE));
                chartsPanel3.add(getChartT("phiZ","Бортовой угол прицеливания","phiZ",3,TYPE_MEASURE));

                JPanel chartsPanel10 = new JPanel(new GridLayout(2,2));
                chartsPanel10.add(getChartT("OmegaX","Угловая скорость phiX","OmegaX",4,TYPE_MEASURE));
                chartsPanel10.add(getChartT("OmegaY","Угловая скорость phiY","OmegaY",5,TYPE_MEASURE));
                chartsPanel10.add(getChartT("OmegaZ","Угловая скорость phiZ","OmegaZ",6,TYPE_MEASURE));

                tab.add("Измерения",chartsPanel3);
                tab.add("Измерения",chartsPanel10);

                tab.add("3D график",getXYZPanel());

                JPanel chartsPanel4 = new JPanel(new GridLayout(3,2));
                chartsPanel4.add(getChartT("phiX","Угол курса","Оценка phiX",0,TYPE_RATING_ANGLE));
                chartsPanel4.add(getChartT("phiY","Угол места","Оценка phiY",1,TYPE_RATING_ANGLE));
                chartsPanel4.add(getChartT("phiZ","Бортовой угол прицеливания","Оценка phiZ",2,TYPE_RATING_ANGLE));
                chartsPanel4.add(getChartT("OmegaX","Угловая скорость phiX","Оценка OmegaX",3,TYPE_RATING_ANGLE));
                chartsPanel4.add(getChartT("OmegaY","Угловая скорость phiY","Оценка OmegaY",4,TYPE_RATING_ANGLE));
                chartsPanel4.add(getChartT("OmegaZ","Угловая скорость phiZ","Оценка OmegaZ",5,TYPE_RATING_ANGLE));

                tab.add("Оценки углов",chartsPanel4);

                JPanel chartsPanel5 = new JPanel(new GridLayout(3,2));
                chartsPanel5.add(getChartT("phiX","Угол курса","Ошибка phiX",0,TYPE_ANGLE_ERR));
                chartsPanel5.add(getChartT("phiY","Угол места","Ошибка phiY",1,TYPE_ANGLE_ERR));
                chartsPanel5.add(getChartT("phiZ","Бортовой угол прицеливания","Ошибка phiZ",2,TYPE_ANGLE_ERR));

                chartsPanel5.add(getChartT("OmegaX","Угловая скорость phiX","Ошибка OmegaX",3,TYPE_ANGLE_ERR));
                chartsPanel5.add(getChartT("OmegaY","Угловая скорость phiY","Ошибка OmegaY",4,TYPE_ANGLE_ERR));
                chartsPanel5.add(getChartT("OmegaZ","Угловая скорость phiZ","Ошибка OmegaZ",5,TYPE_ANGLE_ERR));


                tab.add("Ошибки оценки углов",chartsPanel5);

                JPanel chartsPanel6 = new JPanel(new GridLayout(1,1));
                chartsPanel6.add(getChartT("D","Дальность","Оценка D",0,TYPE_RATING_RANGE));

                tab.add("Оценка дальности",chartsPanel6);

                JPanel chartsPanel7 = new JPanel(new GridLayout(1,1));
                chartsPanel7.add(getChartT("D","Дальность","Ошибка D",0,TYPE_RANGE_ERR));

                tab.add("Ошибка оценки дальности",chartsPanel7);

                JPanel chartsPanel8 = new JPanel(new GridLayout(2,2));

                chartsPanel8.add(getChartT("X","Положение цели X","Оценка X",0,TYPE_RATING_POS));
                chartsPanel8.add(getChartT("Y","Положение цели Y","Оценка Y",1,TYPE_RATING_POS));
                chartsPanel8.add(getChartT("Z","Положение цели Z","Оценка Z",2,TYPE_RATING_POS));

                tab.add("Оценка положения цели",chartsPanel8);

                JPanel chartsPanel9 = new JPanel(new GridLayout(2,2));

                chartsPanel9.add(getChartT("X","Скорость цели X","Оценка скорости X",0,TYPE_RATING_VEL));
                chartsPanel9.add(getChartT("Y","Скорость цели Y","Оценка скорости Y",1,TYPE_RATING_VEL));
                chartsPanel9.add(getChartT("Z","Скорость цели Z","Оценка скорости Z",2,TYPE_RATING_VEL));

                tab.add("Оценка скорости цели",chartsPanel9);
                setContentPane(tab);
            }
        });

    }

    private static JPanel getChartT(String y,String title,String series,int j,int type){

        IntervalXYDataset data1 = createDatasetT(series,j,type);


        final XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
        renderer.setBaseShapesVisible(false);


        XYPlot plot1 = new XYPlot(data1, new NumberAxis("t"), new NumberAxis(y), renderer);
        plot1.setBackgroundPaint(Color.lightGray);
        plot1.setDomainGridlinePaint(Color.white);
        plot1.setRangeGridlinePaint(Color.white);

        boolean legend = data1.getSeriesCount() > 1 ;
        JFreeChart chart1 = new JFreeChart(title,
                JFreeChart.DEFAULT_TITLE_FONT, plot1, legend);
        chart1.setBackgroundPaint(Color.white);
        ChartPanel panel1 = new ChartPanel(chart1);
        JPanel content = new JPanel(new BorderLayout());
        content.setPreferredSize(DEFAULT_CONTENT_SIZE);
        content.add(panel1);
        return content;
    }

    private static IntervalXYDataset createDatasetT(String series,int y,int type) {
        // create dataset 1...
        XYSeriesCollection collection = new XYSeriesCollection();
        collection.addSeries(new XYSeries(series));

        switch (type){
            case TYPE_RATING_ANGLE:
                collection.addSeries(new XYSeries("Истинное значение"));
                break;
            case TYPE_RATING_RANGE:
                collection.addSeries(new XYSeries("Истинное значение"));
                break;
            case TYPE_RATING_POS:
                collection.addSeries(new XYSeries("Истинное значение"));
                break;
            case TYPE_RATING_VEL:
                collection.addSeries(new XYSeries("Истинное значение"));
                break;
            case TYPE_ANGLE_ERR:
                collection.addSeries(new XYSeries("+3 sigma"));
                collection.addSeries(new XYSeries("-3 sigma"));
                break;
            case TYPE_RANGE_ERR:
                collection.addSeries(new XYSeries("+3 sigma"));
                collection.addSeries(new XYSeries("-3 sigma"));
                break;
        }

        for (int i = 0 ; i < simulator.getCount() ; i++){
            switch (type){
                case TYPE_MEASURE :
                    if (i < simulator.Y.size())
                        collection.getSeries(0).add(i * simulator.step, simulator.Y.get(i).getData(y, 0));
                    break;
                case TYPE_MOTION :
                    collection.getSeries(0).add(i * simulator.step, simulator.getX(i).getData(y, 0));
                    break;
                case TYPE_RATING_ANGLE :
                    if (i < simulator.Y.size()){
                        collection.getSeries(0).add(i * simulator.step, toDegrees( filter1.XoList.get(i).getData(y, 0)));
                        collection.getSeries(1).add(i * simulator.step, toDegrees( simulator.Y.get(i).getData(y + 1, 0)));
                    }
                    break;
                case TYPE_RATING_POS :
                    if ( i < ratPos.size()){
                        collection.getSeries(0).add(i * simulator.step, ratPos.get(i).getData(y,0));
                        collection.getSeries(1).add(i * simulator.step, simulator.getX(i).getData(Simulator.SELF_MOTION_SIZE+y,0));
                    }
                    break;
                case TYPE_RATING_VEL:
                    if ( i < ratVel.size()){
                        collection.getSeries(0).add(i * simulator.step, ratVel.get(i).getData(y,0));
                        collection.getSeries(1).add(i * simulator.step, simulator.lV.get(i).getData(y, 0));
                    }
                    break;
                case TYPE_ANGLE_ERR:
                    if (i < simulator.Y.size()){
                        collection.getSeries(0).add(i * simulator.step, filter1.XoList.get(i).getData(y, 0)-simulator.Y.get(i).getData(y+1,0));
                        double sigma =   sqrt(filter1.KoList.get(i).getData(y, y));
                        collection.getSeries(1).add(i * simulator.step,3*sigma);
                        collection.getSeries(2).add(i * simulator.step,-3*sigma);
                    }
                    break;
                case TYPE_RATING_RANGE:
                    if (i < simulator.Y.size()){
                        double d = 0;
                        double di = simulator.Y.get(i).getData(0, 0);
                      // double t = i * simulator.step;
                        double t =  simulator.step;
                        for(int j = 0 ; j < filter2.XoList.get(i).getRow(); j++){
                            d += filter2.XoList.get(i).getData(j,0)*pow(t,j)/fact(j);
                        }
                        t = i * simulator.step;
                        collection.getSeries(0).add(t, d);
                        collection.getSeries(1).add(t, di);
                    }
                    break;
                case TYPE_RANGE_ERR:
                    if (i < simulator.Y.size()){
                        double d = 0;
                      //  double t = i * simulator.step;
                        double t =  simulator.step;

                        for(int j = 0 ; j < filter2.XoList.get(i).getRow(); j++){
                            d += filter2.XoList.get(i).getData(j,0)*pow(t,j)/fact(j);
                        }
                        t = i * simulator.step;
                        collection.getSeries(0).add(t,simulator.Y.get(i).getData(0, 0)- d);
                        double sigma =   filter2.getSigma(i);
                        collection.getSeries(1).add(i * simulator.step,3*sigma);
                        collection.getSeries(2).add(i * simulator.step,-3*sigma);
                    }
                    break;
            }

        }

        return collection;
    }

    static double fact(int n){
        if (n == 0) return 1;
        return n * fact(n-1);
    }
    
    private static JPanel getXYZPanel(){
        XYZDataset data3 = createDatasetXYZ();
        Chart3D chart = Chart3DFactory.createScatterChart("TargetMotion",
                "", data3, "X", "Y", "Z");
        XYZPlot plot3 = (XYZPlot) chart.getPlot();
        ScatterXYZRenderer renderer = (ScatterXYZRenderer) plot3.getRenderer();
        renderer.setSize(0.1);

        chart.setViewPoint(ViewPoint3D.createAboveLeftViewPoint(40));
        ChartPanel3D chartPanel = new ChartPanel3D(chart);
        chartPanel.zoomToFit(DEFAULT_CONTENT_SIZE);
        JPanel content = new JPanel(new BorderLayout());
        content.setPreferredSize(DEFAULT_CONTENT_SIZE);
        content.add(new DisplayPanel3D(chartPanel));
        return content;
    }

    private static JPanel getXYPanel(String x,String y,String title,String series,int i,int j){
        IntervalXYDataset data1 = createDatasetXY(series,i,j);

        final XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
        Shape shape = new Rectangle(1,1);
        renderer.setBaseShape(shape);
        renderer.setSeriesLinesVisible(0, false);


        XYPlot plot1 = new XYPlot(data1, new NumberAxis(x), new NumberAxis(y), renderer);
        plot1.setBackgroundPaint(Color.lightGray);
        plot1.setDomainGridlinePaint(Color.white);
        plot1.setRangeGridlinePaint(Color.white);


        JFreeChart chart1 = new JFreeChart(title,
                JFreeChart.DEFAULT_TITLE_FONT, plot1, false);
        chart1.setBackgroundPaint(Color.white);
        ChartPanel panel1 = new ChartPanel(chart1);
        JPanel content = new JPanel(new BorderLayout());
        content.setPreferredSize(DEFAULT_CONTENT_SIZE);
        content.add(panel1);
        return content;
    }



    private static IntervalXYDataset createDatasetXY(String series,int x,int y) {
        // create dataset 1...
        XYSeries series1 = new XYSeries(series);
        for (int i = 0 ; i < simulator.getCount() ; i++){
            series1.add(simulator.getX(i).getData(x,0),simulator.getX(i).getData(y,0));
        }
        return new XYSeriesCollection(series1);

    }

    private static XYZDataset createDatasetXYZ() {

        XYZSeries self = new XYZSeries("self");
        XYZSeries target = new XYZSeries("target");
        XYZSeries rattarget = new XYZSeries("rattarget");
        XYZSeries x1 = new XYZSeries("x1");
        XYZSeries y1 = new XYZSeries("y1");
        XYZSeries z1 = new XYZSeries("z1");



        for (int i = 0 ;  i < simulator.getCount(); i+=2){
            self.add(simulator.getX(i).getData(0,0),simulator.getX(i).getData(1,0),simulator.getX(i).getData(2,0));
            target.add(simulator.getX(i).getData(Simulator.SELF_MOTION_SIZE+0,0),
                    simulator.getX(i).getData(Simulator.SELF_MOTION_SIZE+1,0),
                    simulator.getX(i).getData(Simulator.SELF_MOTION_SIZE+2,0));
        }
        for (int i = 0 ; i < ratPos.size(); i++){
            rattarget.add(ratPos.get(i).getData(0,0),ratPos.get(i).getData(1,0),ratPos.get(i).getData(2,0));
        }
        for (int i = 1 ; i < 500 ; i+=10){
            int n = 1;
            x1.add(simulator.getX(simulator.getCount()-1).getData(0,0)*n+simulator.x1.getData(0,0)*i,
                    simulator.getX(simulator.getCount()-1).getData(1,0)*n+simulator.x1.getData(1,0)*i,
                    simulator.getX(simulator.getCount()-1).getData(2,0)*n+simulator.x1.getData(2,0)*i);

            y1.add(simulator.getX(simulator.getCount()-1).getData(0,0)*n+simulator.y1.getData(0,0)*i,
                    simulator.getX(simulator.getCount()-1).getData(1,0)*n+simulator.y1.getData(1,0)*i,
                    simulator.getX(simulator.getCount()-1).getData(2,0)*n+simulator.y1.getData(2,0)*i);

            z1.add(simulator.getX(simulator.getCount()-1).getData(0,0)*n+simulator.z1.getData(0,0)*i,
                    simulator.getX(simulator.getCount()-1).getData(1,0)*n+simulator.z1.getData(1,0)*i,
                    simulator.getX(simulator.getCount()-1).getData(2,0)*n+simulator.z1.getData(2,0)*i);
        }

        XYZSeriesCollection dataset = new XYZSeriesCollection();
        dataset.add(target);
        dataset.add(self);
      //  dataset.add(rattarget);
        dataset.add(x1);
        dataset.add(y1);
        dataset.add(z1);



        return dataset;
    }




}

