package com.edu.sys.models;

import com.edu.sys.matrix.Matrix;
import com.edu.sys.matrixoperations.Rotate;
import com.edu.sys.vectoroperations.Norm;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;

import static java.lang.Math.*;

/**
 * Created by Nixy on 30.04.14.
 */
public class TargetMotion extends AbstractModel {

    public static final double MAX_V =  800*1000./3600;   // Максимальная скорость ЛА

    double[] params;                   // Текущие параметры маневра цели
    HashMap<Double,double[]>  map ;   // Хранилище параметров маневров цели

    Matrix V;             // направление потребной скорости цели

    public TargetMotion(Matrix initCondition, double step,HashMap<Double,double[]> map) {
        super(initCondition, step);
        this.map = map;
    }

    @Override
    public boolean isEnough(Object... params) {
        return false;
    }

    @Override
    public Matrix F(double t, Matrix Xn) {
        double x = Xn.getData(0,0);
        double y = Xn.getData(1,0);
        double z = Xn.getData(2,0);
        double teta = Xn.getData(3,0);

        // cкорость в плоскости вращения
        V = new Matrix(new double[][]{{0},{-cos(teta)},{-sin(teta)}});
        getV(t);

        // модуль угловой скорости
        double omega = MAX_V/params[2];


        double dx = V.getData(0,0);
        double dy = V.getData(1,0);
        double dz = V.getData(2,0);
        double dteta = omega;

        Xn.setData(0, 0, dx);
        Xn.setData(1, 0, dy);
        Xn.setData(2, 0, dz);
        Xn.setData(3, 0, dteta);


        return Xn;
    }

   /*
   *  Метод отыскания направления потребной скорости
   * */
    public  void getV (double t) {
        Double key = new BigDecimal(t).setScale(1, RoundingMode.DOWN).doubleValue();
        if (map.containsKey(key))
        // получение текущего маневра цели
            params = map.get(key);

        V = Rotate.Rz(V,params[0]);               // поворот вокруг ОZ
        V = Rotate.Ry(V, params[1]);              // пововрот вокруг ОY'
        V = Norm.calc(V);                         // нормировка
        V = V.getWithFactor(MAX_V);               // задание длины
    }

}
