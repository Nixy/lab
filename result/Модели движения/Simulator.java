package com.edu.sys.models;

import com.edu.sys.matrix.Matrix;
import com.edu.sys.matrixoperations.*;
import com.edu.sys.quaternion.Quaternion;
import com.edu.sys.vectoroperations.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import static java.lang.Math.*;

/**
 * Created by Nixy on 03.05.14.
 */
public class Simulator extends AbstractModel {

    public static final int SELF_MOTION_SIZE = 6;
    public static final int TARGET_MOTION_SIZE = 4;

    public List<Matrix> Y = new ArrayList<Matrix>();
    public List<Matrix> S = new ArrayList<Matrix>();

    public Matrix x1;
    public Matrix y1;
    public Matrix z1;

    public List<Matrix> lV = new ArrayList<Matrix>();
    Matrix selfV;
    SelfMotion selfMotion;
    TargetMotion targetMotion;

    public Simulator(Matrix initCondition, double step,HashMap<Double,double[]> map) {
        super(initCondition, step);
        Matrix selfInit = new Matrix(SELF_MOTION_SIZE,1);
        for (int i = 0 ; i < SELF_MOTION_SIZE ; i++)
            selfInit.setData(i,0,initCondition.getData(i,0));
        Matrix targetInit = new Matrix(TARGET_MOTION_SIZE,1);
        for (int i = 0 ; i < TARGET_MOTION_SIZE ; i++)
            targetInit.setData(i,0,initCondition.getData(SELF_MOTION_SIZE+i,0));

        selfMotion = new SelfMotion(selfInit,step);
        targetMotion = new TargetMotion(targetInit,step,map);

        Matrix selfX = initCondition.getVector(0,0,3,Matrix.TYPE_COL);
        Matrix target = initCondition.getVector(0,SELF_MOTION_SIZE,SELF_MOTION_SIZE+3,Matrix.TYPE_COL);
        selfV = Sub2.calc(target,selfX);
        selfV = Norm.calc(selfV);
    }

    @Override
    public boolean isEnough(Object... params) {
        return false;
    }

    @Override
    public Matrix F(double t, Matrix Xn) {
        Matrix selfXn = new Matrix(SELF_MOTION_SIZE,1);
        for (int i = 0 ; i < SELF_MOTION_SIZE ; i++)
            selfXn.setData(i,0,Xn.getData(i,0));
        Matrix targetXn = new Matrix(TARGET_MOTION_SIZE,1);
        for (int i = 0 ; i < TARGET_MOTION_SIZE ; i++)
            targetXn.setData(i,0,Xn.getData(SELF_MOTION_SIZE+i,0));
   //    for (int i = 0 ; i < 3; i++)
    //        selfXn.setData(SELF_MOTION_SIZE+i-3,0,targetXn.getData(i,0));
        selfMotion.F(t,selfXn);
        targetMotion.F(t,targetXn);

        for (int i = 0 ; i < SELF_MOTION_SIZE ; i++)
            Xn.setData(i,0,selfXn.getData(i,0));

        for (int i = 0 ; i < TARGET_MOTION_SIZE ; i++)
            Xn.setData(SELF_MOTION_SIZE+i,0,targetXn.getData(i,0));

    return Xn;
}

    @Override
    public void setX(Matrix X) {
        super.setX(X);


        Matrix selfX = X.getVector(0,0,3,Matrix.TYPE_COL);
        Matrix target = X.getVector(0,SELF_MOTION_SIZE,SELF_MOTION_SIZE+3,Matrix.TYPE_COL);

        Matrix D = Sub2.calc(target,selfX);
        S.add(D);
        double d = Module.calc(D);    // модуль дальности до цели
        selfV = Norm.calc(selfV);
        D = Norm.calc(D);     // направление дальности до цели

        // определение углов ориентации связанной СК
        double alpha = atan2(selfV.getData(1,0),selfV.getData(0,0));
        double delta = -atan2(selfV.getData(2,0),sqrt(pow(selfV.getData(0,0),2)+pow(selfV.getData(1,0),2)));

        x1 = new Matrix(new double[][]{{1},{0},{0}});
        y1 = new Matrix(new double[][]{{0},{1},{0}});
        z1 = new Matrix(new double[][]{{0},{0},{1}});

        Quaternion q = new Quaternion(alpha,new Matrix(new double[][]{{0},{0},{1}}));
        Matrix Q = q.toMatrix();

        x1 = Multi.calc(Q,x1);
        y1 = Multi.calc(Q,y1);
        z1 = Multi.calc(Q, z1);

        q = new Quaternion(delta,y1);
        Q = q.toMatrix(); 
        x1 = Multi.calc(Q,x1);
        y1 = Multi.calc(Q,y1);
        z1 = Multi.calc(Q,z1);

        // Измерения
        double fiZ = Angle.calc(z1,D);
        double fiY = Angle.calc(y1,D);
        double fiX = Angle.calc(x1,D);

        double omegaZ = Y.size() > 1 ? (fiZ - Y.get(Y.size()-1).getData(3,0))/step :  0;
        double omegaY = Y.size() > 1 ? (fiY - Y.get(Y.size()-1).getData(2,0))/step :  0;
        double omegaX = Y.size() > 1 ? (fiX - Y.get(Y.size()-1).getData(1,0))/step :  0;

        Matrix y = new Matrix(new double[][]{{d},{fiX},{fiY},{fiZ},{omegaX},{omegaY},{omegaZ}});

        S.add(x1);
        S.add(y1);
        S.add(z1);

        Y.add(y);
        lV.add(targetMotion.V);
    }
}
