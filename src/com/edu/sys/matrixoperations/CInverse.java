package com.edu.sys.matrixoperations;


import com.edu.sys.matrix.Matrix;

/**
 * Created with IntelliJ IDEA.
 * User: Миша
 * Date: 18.10.13
 * Time: 17:39
 * To change this template use File | Settings | File Templates.
 */
public class CInverse {
    public static Matrix calc(Matrix A){

        Matrix res,L,LT;
        Matrix tmp = invertTriangleMatrix(A);
        res = new Matrix(A.getRow(),A.getCol());
        L =   new Matrix(A.getRow(),A.getCol());
        LT = new Matrix(A.getRow(),A.getCol());
        for (int i=0; i<A.getRow();i++)
            for (int j=0; j<=i; j++)
                if (i==j){
                    for (int k=0; k<i;k++)
                        L.setData(i,i, L.getData(i,i)+L.getData(i,k)*L.getData(i,k));
                    L.setData(i,i, Math.sqrt(A.getData(i,i)-L.getData(i,i)));
                }
                else {
                    for (int k=0; k < j;k++)
                        L.setData(i,j, L.getData(i,j)+L.getData(i,k)*L.getData(j,k));
                    L.setData(i,j, (A.getData(i,j)-L.getData(i,j))/L.getData(j,j));
                }
        L =  Transpose.calc(L);
        L = invertTriangleMatrix(L);
        LT = Transpose.calc(L);
        res = Multi.calc(L,LT);

        return res;
    }

    private static Matrix checResForNan(Matrix res,Matrix tmp) {
        for (int i = 0 ; i < res.getRow(); i ++){
            for (int j = 0 ; j < res.getCol(); j ++){
               if( Double.compare(res.getData(i,j),Double.NaN) ==  0 )  return tmp;
            }
        }
        return res;
    }


    private static Matrix invertTriangleMatrix(Matrix m){
        Matrix E = new Matrix(m);
        Matrix M = new Matrix(m);
        for (int i = 0 ; i < E.getRow(); i ++){
            for (int j = 0 ; j < E.getCol(); j ++){
                E.setData(i, j, (i == j) ? 1 : 0);
            }
        }
        for (int i = 0 ; i < m.getRow(); i++){
            double div = m.getData(i,i);
            if (div != 0){
               for (int j = 0; j < m.getCol() ; j++){
                   E.setData(i,j,E.getData(i,j)/div);
                   M.setData(i,j,M.getData(i,j)/div);
               }
               for (int k = 0 ; k < m.getRow(); k++ ){
                   if (k != i){
                       double mult = M.getData(k,i);
                       for (int j = 0 ; j < m.getCol(); j++){
                           E.setData(k,j,E.getData(k,j)-E.getData(i,j)*mult);
                           M.setData(k,j,M.getData(k,j)-M.getData(i,j)*mult);
                       }
                   }
               }
            }  else return m;
        }
        return E;
    }
}
