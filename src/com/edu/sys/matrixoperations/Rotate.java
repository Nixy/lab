package com.edu.sys.matrixoperations;

import com.edu.sys.matrix.Matrix;
import static java.lang.Math.*;

/**
 * Created by Nixy on 30.04.14.
 */
public class Rotate {

    public static Matrix getAx(double angle){
        return new Matrix( new double[][]{
                {1,0,0},
                {0,cos(angle),-sin(angle)},
                {0,sin(angle),cos(angle)}
        });
    }

    public static Matrix getAy(double angle){
        return new Matrix( new double[][]{
                {cos(angle),0,sin(angle)},
                {0,1,0},
                {-sin(angle),0,cos(angle)}
        });
    }

    public static Matrix getAz(double angle){
        return new Matrix( new double[][]{
                {cos(angle),-sin(angle),0},
                {sin(angle),cos(angle),0},
                {0,0,1}
        });
    }

    public static Matrix A(double a,double b,double c){
        Matrix result = Multi.calc(getAy(b),getAz(c));
        result = Multi.calc(getAx(a),result);
        return result;
    }

    public static Matrix AT(double a,double b,double c){
        Matrix result = Multi.calc(getAy(b),getAz(c));
        result = Multi.calc(getAx(a),result);
        return Transpose.calc(result);
    }

    public static Matrix Rx(Matrix a,double angle){
        Matrix Ax = getAx(angle);
        return Multi.calc(Ax,a);
    }

    public static Matrix Ry(Matrix a,double angle){
        Matrix Ax = getAy(angle);
        return Multi.calc(Ax,a);
    }

    public static Matrix Rz(Matrix a,double angle){
        Matrix Ax = getAz(angle);
        return Multi.calc(Ax,a);
    }

    public static Matrix RxT(Matrix a,double angle){
        Matrix Ax = Transpose.calc(getAx(angle));
        return Multi.calc(Ax,a);
    }

    public static Matrix RyT(Matrix a,double angle){
        Matrix Ax = Transpose.calc(getAy(angle));
        return Multi.calc(Ax,a);
    }

    public static Matrix RzT(Matrix a,double angle){
        Matrix Ax = Transpose.calc(getAz(angle));
        return Multi.calc(Ax,a);
    }

}
