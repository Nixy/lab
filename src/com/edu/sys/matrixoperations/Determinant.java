package com.edu.sys.matrixoperations;


import com.edu.sys.matrix.Matrix;

/**
 * Created with IntelliJ IDEA.
 * User: Миша
 * Date: 23.09.13
 * Time: 19:44
 * To change this template use File | Settings | File Templates.
 */
public class Determinant {
    public static double calc(Matrix m){
        double res=0;

        if (m.getCol()!=m.getRow())
            return Double.NaN;

        if (m.getRow()==1){
          return m.getData(0,0);
        }
        for (int i=0; i<m.getCol(); i++){
            if (m.getCol()==2) {
                res = m.getData(0,0)*m.getData(1,1)-m.getData(0,1)*m.getData(1,0);
                return res;
            }
            else {
                Matrix tmp = new Matrix (m.getRow()-1,m.getCol()-1);

                for (int y = 1; y<m.getRow(); y++)
                    for (int k = 0, x = 0; k<m.getCol(); k++)
                        if ( k!=i ) {
                            tmp.setData(y-1,x, m.getData(y,k));
                            x++;
                        }

                res += Math.pow(-1.,i)*m.getData(0,i)*calc(tmp);
            }
        }
        return res;
    }
}
