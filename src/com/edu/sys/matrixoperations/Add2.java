package com.edu.sys.matrixoperations;

import com.edu.sys.matrix.Matrix;

/**
 * Created with IntelliJ IDEA.
 * User: Nixy
 * Date: 22.09.13
 * Time: 12:24
 *
 * Класс Add2
 * Cложение матрицы А и B и сохранение результата в  новую матрицу R
 */
public class Add2  {
    public static Matrix calc(Matrix... args){
        if (args[0] != null && args[1] != null)
            if (args[0].isSameSize(args[1])){
                Matrix result = new Matrix().copy(args[0]);
                for (int i = 0 ; i < result.getRow(); i++)
                    for ( int j = 0 ; j < result.getCol(); j++)
                        result.setData(i,j,result.getData(i,j)+args[1].getData(i,j));
                return result;
            }
        return null;
    };
}
