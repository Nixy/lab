package com.edu.sys.matrixoperations;


import com.edu.sys.matrix.Matrix;

/**
 * Created with IntelliJ IDEA.
 * User: Nixy
 * Date: 22.09.13
 * Time: 12:30
 *
 * Класс AddWith
 * Сложение матрицы А и B сохранение результата в матрице A
 */
public class AddWith {
    public static Matrix calc(Matrix... args){
        if (args[0] != null && args[1] != null)
            if (args[0].isSameSize(args[1])){
                for (int i = 0 ; i < args[0].getRow(); i++)
                    for ( int j = 0 ; j < args[0].getCol(); j++)
                        args[0].setData(i,j,args[0].getData(i,j)+args[1].getData(i,j));
                return args[0];
            }
        return null;
    };
}
