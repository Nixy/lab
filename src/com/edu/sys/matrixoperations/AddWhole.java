package com.edu.sys.matrixoperations;


import com.edu.sys.matrix.Matrix;

/**
 * Created with IntelliJ IDEA.
 * User: Nixy
 * Date: 22.09.13
 * Time: 12:36
 *
 * Класс AddWhole
 * Сложение всех матриц из args и сохранение результата в новой матрице R
 */
public class AddWhole {
    public static Matrix calc(Matrix... args){
        boolean flag = true;
        for (int n = 0 ; n < args.length ; n ++)
            flag = flag && args[n] != null;
        if (flag){
            for (int n = 0 ; n < args.length-1 ; n ++)
                flag = flag && args[n].isSameSize(args[n+1]);
            if (flag){
                 Matrix result = new Matrix().copy(args[0]);
                 for (int n = 1 ; n < args.length ; n ++)
                     for (int i = 0 ; i < result.getRow(); i++)
                         for ( int j = 0 ; j < result.getCol(); j++)
                             result.setData(i,j,result.getData(i,j)+args[n].getData(i,j));
                 return result;
            }
        }
        return null;
    };
}
