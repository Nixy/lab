package com.edu.sys.integrators;

import com.edu.sys.matrix.Matrix;
import com.edu.sys.matrixoperations.AddWholeWith;
import com.edu.sys.matrixoperations.AddWith;
import com.edu.sys.models.AbstractModel;
import static java.lang.Math.* ;

/**
 * Created by RoadNoah on 30.04.2014.
 */

public class DormanPrince extends AbstractIntegrator {

    Matrix K1,K2,K3,K4,K5,K6,K7;
    Matrix Y0,Y,Y4,Y5,tmp;
    double h, inh, maxE;

    public DormanPrince(AbstractModel m, double start, double end, double step, double maxE){
        super(m, start, end, step);
        K1 = new Matrix();
        K2 = new Matrix();
        K3 = new Matrix();
        K4 = new Matrix();
        K5 = new Matrix();
        K6 = new Matrix();
        K7 = new Matrix();
        tmp = new Matrix();
        Y0 = new Matrix();
        Y4 = new Matrix();
        Y5 = new Matrix();
        Y = new Matrix();
        this.maxE = maxE;
    }

    public double u() {
        double v = 1;
        double tmp = 0;
        while ((1+v)>1) {
            tmp = v;
            v = v/2;
        }
        return tmp;
    }

    public double e (Matrix Y0, Matrix Y4, Matrix Y5) {
       double sum = 0;
       for (int i = 0 ; i < model.getVectorSize(); i++){
           double i1 = Y4.getData(i,0)-Y5.getData(i,0);
           double i2 = max(abs(Y0.getData(i,0)),2*u()/maxE);
           double i3 = max(abs(Y4.getData(i,0)),i2);
           double i4 = max(pow(10,-5),i3);
           sum += pow(step*(i1)/i4,2);
       }
        return sqrt(sum/model.getVectorSize());
    }

    public double newStep  (Matrix Y0, Matrix Y4, Matrix Y5) {
        double err = e(Y0, Y4, Y5);
        double mi =min(5, pow(err / maxE, 1. / 5) / 0.9);
        double ma = max(0.1, mi);
        return step / (ma);
    }

    @Override
    public void integrate() {


        double t = start;
        double cStp = step;
        double b1,b2,b3,b4,b5,b6;

        Y4 = new Matrix().copy(model.getX(model.getCount()-1));

        for (double i = start ; i < end ; i+=inh){
            Y0.copy(Y4);

            do {
                inh = step;

                tmp.copy(Y0);
                K1.copyWithFactor(model.F(i, tmp), step);

                tmp.copyWithFactor(K1, 0.2).copy(AddWith.calc(tmp, Y0));
                K2.copyWithFactor(model.F(i + step / 5, tmp), step);

                tmp.copy(Y0).copy(AddWholeWith.calc(tmp,
                        K1.getWithFactor(3. / 40),
                        K2.getWithFactor(9. / 40)));
                K3.copyWithFactor(model.F(i + 2 * step / 10, tmp), step);

                tmp.copy(Y0).copy(AddWholeWith.calc(tmp,
                        K1.getWithFactor(44. / 45),
                        K2.getWithFactor(-56. / 15),
                        K3.getWithFactor(32. / 9)));
                K4.copyWithFactor(model.F(i + 4 * step / 5, tmp), step);

                tmp.copy(Y0).copy(AddWholeWith.calc(tmp,
                        K1.getWithFactor(19372. / 6561),
                        K2.getWithFactor(-25360. / 2187),
                        K3.getWithFactor(64448. / 6561),
                        K4.getWithFactor(-212. / 729)));
                K5.copyWithFactor(model.F(i + 8 * step / 9, tmp), step);

                tmp.copy(Y0).copy(AddWholeWith.calc(tmp,
                        K1.getWithFactor(9017. / 3168),
                        K2.getWithFactor(-355. / 33),
                        K3.getWithFactor(46732. / 5247),
                        K4.getWithFactor(49. / 176),
                        K5.getWithFactor(-5103. / 18656)));
                K6.copyWithFactor(model.F(i + step, tmp), step);

                tmp.copy(Y0).copy(AddWholeWith.calc(tmp,
                        K1.getWithFactor(35. / 384),
                        K2.getWithFactor(0),
                        K3.getWithFactor(500. / 1113),
                        K4.getWithFactor(125. / 192),
                        K5.getWithFactor(-2187. / 6784),
                        K6.getWithFactor(11. / 84)));
                K7.copyWithFactor(model.F(i + step, tmp), step);

                Y4.copy(Y0).copy(AddWholeWith.calc(Y4,
                        K1.getWithFactor(35. / 384),
                        K2.getWithFactor(0),
                        K3.getWithFactor(500. / 1113),
                        K4.getWithFactor(125. / 192),
                        K5.getWithFactor(-2187. / 6784),
                        K6.getWithFactor(11. / 84),
                        K7.getWithFactor(0)));

                Y5.copy(Y0).copy(AddWholeWith.calc(Y5,
                        K1.getWithFactor(5179./57600),
                        K2.getWithFactor(0),
                        K3.getWithFactor(7571./16695),
                        K4.getWithFactor(393./640),
                        K5.getWithFactor(-92097./339200),
                        K6.getWithFactor(187./2100),
                        K7.getWithFactor(1./40)));

                step = newStep(Y0, Y4, Y5);

                if (model.isEnough(step))
                    i = end;

            } while  (e(Y0,Y4,Y5) > maxE);

            while (t <(i+inh)){
                t = t+cStp;
                double teta = (t-i)/inh;
                b1 = teta*(1+teta*(-1337./480+teta*(1039./360+teta*(-1163./1152))));
                b2 = 0;
                b3 = 100*(teta)*(teta)*(1054./9275+teta*(-4682./27825+teta*(379./5565)))/3;
                b4 = -5*(teta)*(teta)*(27./40+teta*(-9./5+teta*(83./96)))/2;
                b5 = 18225*(teta)*(teta)*(-3./250+teta*(22./375+teta*(-37./600)))/848;
                b6 = -22*(teta)*(teta)*(-3./10+teta*(29./30+teta*(-17./24)))/7;

                Y.copy(Y0).copy(AddWholeWith.calc(Y,
                        K1.copyWithFactor(K1, b1),
                        K2.copyWithFactor(K2, b2),
                        K3.copyWithFactor(K3, b3),
                        K4.copyWithFactor(K4, b4),
                        K5.copyWithFactor(K5, b5),
                        K6.copyWithFactor(K6, b6)));

               model.setX(new Matrix(Y));
            }


        }

    }

}

