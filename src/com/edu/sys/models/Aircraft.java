package com.edu.sys.models;

import com.edu.sys.matrix.Matrix;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

/**
 * Created by Nixy on 03.05.14.
 */
public class Aircraft extends AbstractModel {

    private static final double MAX_MV = 200*1000./3600 ;  // Максимальная скорость ЛА
    private double targetX;
    private double targetY;
    private double targetZ;

    public Aircraft(Matrix initCondition, double step, Matrix target) {
        super(initCondition, step);
        targetX = target.getData(0,0);
        targetY = target.getData(1,0);
        targetZ = target.getData(2,0);
    }

    @Override
    public boolean isEnough(Object... params) {
        return false;
    }

    public Matrix V;

    @Override
    public Matrix F(double t, Matrix Xn) {
        //фазовый вектор ЛА
        double x = Xn.getData(0,0);
        double y = Xn.getData(1,0);
        double z = Xn.getData(2,0);

        double d = sqrt(pow(x - targetX, 2) + pow(y - targetY, 2) + pow(z - targetZ, 2));
        // Направление скорости
        V  = new Matrix(new double[][]{{(targetX-x)/d},{(targetY-y)/d},{(targetZ-z)/d}});
        V = V.getWithFactor(MAX_MV);

        double dx = V.getData(0,0);
        double dy = V.getData(1,0);
        double dz = V.getData(2,0);

        Xn.setData(0,0,dx);
        Xn.setData(1,0,dy);
        Xn.setData(2,0,dz);

        return Xn;
    }
}
