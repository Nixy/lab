package com.edu.sys.models;

import com.edu.sys.matrix.Matrix;
import com.edu.sys.vectoroperations.Module;
import com.edu.sys.vectoroperations.Norm;

import static java.lang.Math.*;

/**
 * Created by Nixy on 03.05.14.
 */
public class SelfMotion extends AbstractModel {

    private static final double MAX_MV = 290*1000./3600 ;  // Максимальная скорость ЛА

    public SelfMotion(Matrix initCondition, double step) {
        super(initCondition, step);
    }

    @Override
    public boolean isEnough(Object... params) {
        return false;
    }

    public Matrix V;

    @Override
    public Matrix F(double t, Matrix Xn) {
        double x = Xn.getData(0,0);
        double y = Xn.getData(1,0);
        double z = Xn.getData(2,0);


        double xc = Xn.getData(3,0);
        double yc = Xn.getData(4,0);
        double zc = Xn.getData(5,0);

        double d = sqrt(pow(x - xc, 2) + pow(y - yc, 2) + pow(z - zc, 2));
        // Направление скорости
        V  = new Matrix(new double[][]{{(xc-x)/d},{(yc-y)/d},{(zc-z)/d}});
        V = Norm.calc(V);
        V = V.getWithFactor(MAX_MV);

        double dx = V.getData(0,0);
        double dy = V.getData(1,0);
        double dz = V.getData(2,0);

        double dxc = 0;
        double dyc = 0;
        double dzc = 0;

        Xn.setData(0,0,dx);
        Xn.setData(1,0,dy);
        Xn.setData(2,0,dz);
        Xn.setData(3,0,dxc);
        Xn.setData(4,0,dyc);
        Xn.setData(5,0,dzc);

        return Xn;
    }
}
