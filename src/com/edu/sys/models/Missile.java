package com.edu.sys.models;

import com.edu.sys.matrix.Matrix;
import com.edu.sys.quaternion.Quaternion;

import java.util.Random;

/**
 * Author Grinch
 * Date: 11.06.13
 * Time: 9:05
 */
public class Missile extends AbstractModel {
    Random random = new Random();

    public Missile(Matrix initVector, double delta) {
        super(initVector, delta);
        generate();
    }

    @Override
    public boolean isEnough(Object... params) {
        return false;
    }


    @Override
    public void setX(Matrix matrix){
        super.setX(matrix);
        generate();
    }

    double rDelta;
    double rny;
    double rnx;
    double rnz;
    double rnvy;
    double rnvx;
    double rnvz;

    void generate(){
        rDelta = random.nextGaussian()/Math.pow(step,1./2);
        rny = random.nextGaussian()/Math.pow(step,1./2);
        rnx = random.nextGaussian()/Math.pow(step,1./2);
        rnz = random.nextGaussian()/Math.pow(step,1./2);
        rnvy = random.nextGaussian()/Math.pow(step,1./2);
        rnvx = random.nextGaussian()/Math.pow(step,1./2);
        rnvz = random.nextGaussian()/Math.pow(step,1./2);
    }

    double TDelta = 10;
    double kDelta = Math.sqrt(2.);

    double m = 800 ; // масса

		/*
		 * коэффициент аэродинамического лобового сопротивления помноженый
		 * на характеристическая площадь (площадь миделя) УАСП, м2
		 */
    double CxS = 0.7 ;
    double P = 1E5 ; // модуль тяги двигателя
    double omegam = 2.7 ;  // модуль угловой скорости тяги двигателя

    double Tnx = 0.01;
    double knx = 1.414213562;

    double Tnvx = 0.00111111;
    double knvx = 0.1555634919;

    double Tny =  Tnx;
    double Tnz =  Tnx;
    double Tnvy =  Tnvx;
    double Tnvz =  Tnvx;
    double kny =  knx;
    double knz =  knx;
    double knvy =  knvx;
    double knvz =  knvx;

    double Tomega = 0.03;
    double komega = 1;
    double targetCord[] = new double[6];

    double  p(double h){
        double H[] = {0 ,20000 ,60000 ,100000 ,150000 ,300000 ,600000 ,900000};
        double A[] = {1.225 ,0.891E-1 ,2.578E-4 ,4.061E-7 ,2.130E-9 ,4.764E-11 ,8.726E-13 ,6.367E-14};
        double K2[] = {-0.2639E-8 ,0.4407E-9 ,-0.2560E-8 ,0.1469E-8 ,0.8004E-10 ,0.7111E-11 ,0.1831E-11 ,0};
        double K1[] = {0.7825E-4 ,0.16375E-3 ,0.5905E-4 ,0.1787E-3 ,0.3734E-4 ,0.1547E-4 ,0.9280E-5 ,0.9540E-5};

        for (int i = 7; i > -1; i--) {
            if ( h > H[i]) {
                double p = A[i]*(1 + delta())*Math.exp(K2[i]*Math.pow((h-H[i]),2)-K1[i]*(h-H[i]));
                return p;
            }
        }
        double p = A[0]*(1 + delta())*Math.exp(K2[0]*Math.pow((h-H[0]),2)-K1[0]*(h-H[0]));
        return p;
    }


    private double delta(){
        return X.get(X.size()-1).getData(6,0);
    }


    public void setMeasure(Matrix vec){
        for (int i = 0; i < 3; i++) {
            targetCord[i] = vec.getData(i, 0);
        }
        double V0[] = {120,10,190};
        for (int i = 3; i < 6; i++) {
            targetCord[i] = V0[i-3];
        }
    }

    @Override
    public Matrix F(double t, Matrix args) {
        //Matrix result = new Matrix(1,20);
        double g = 9.81;
        double F1 = -m*g;//сила гравитации
        double R[] = new double[3]; // вектор Аэродинамических сил

        //===================================
        //======== Вектор состояния =========
        //===================================

        double x = args.getData(0, 0);
        double y = args.getData(1, 0);
        double z = args.getData(2, 0);
        double r = Math.sqrt(x*x+y*y+z*z);
        Matrix Vxyz = new Matrix (1,3);
        for (int i = 0; i < 3; i++) {
            Vxyz.setData(i, 0, args.getData(i, 0) / r);
        }

        double qx = args.getData(13, 0);
        double qy = args.getData(14, 0);
        double qz = args.getData(15, 0);
        double qphi = args.getData(16, 0);

        double omegax = args.getData(17, 0);
        double omegay = args.getData(18, 0);
        double omegaz = args.getData(19, 0);

        Quaternion quater = new Quaternion(qphi,qx,qy,qz); //кватернион поворота
        Quaternion omegaQ = new Quaternion(0,omegax,omegay,omegaz); // кватернион уголовой
        // скорости
        double vx = args.getData(3,0);
        double vy = args.getData(4, 0);
        double vz = args.getData(5, 0);
        double vr = Math.sqrt(vx*vx+vy*vy+vz*vz);

        double delta = args.getData(6, 0);       // атмосферная случаная величина
        double nx = args.getData(7, 0);          // шумы
        double ny = args.getData(8, 0);
        double nz = args.getData(9, 0);

        double nvx = args.getData(10, 0);
        double nvy = args.getData(11, 0);
        double nvz = args.getData(12, 0);
        double tV[] = new double[3];
        tV[0] = targetCord[3]+nvx;
        tV[1] = targetCord[4]+nvy;
        tV[2] = targetCord[5]+nvz;
        double tvr =  Math.sqrt(tV[0]*tV[0]+tV[1]*tV[1]+tV[2]*tV[2]);

        //===================================
        // Расчет аэродинамических сил
        //===================================
        for (int i = 0; i < 3; i++) {
            double pr = p(y);
            R[i] = -CxS*p(y)*vr*args.getData(i + 3, 0)/2;
        }

        //===================================
        //======== Функции правых частей =========
        //===================================

        double f = 0;
        if (Math.abs(t-1) < 1E-2) {
            f = t;
        }
        f++;
        Quaternion quater2 = quater.multiplication(Vxyz);
        Matrix PV  = quater2.multiplication(quater.conjugate()).getIm(Quaternion.TYPE_COL);
        Matrix ePV = new Matrix(1,3);
        double PVr = 0;
        for (int i = 0; i < 3; i++) {
            PV.setData(i, 0, PV.getData(i, 0) * P); 			 // Вектор тяги
            PVr += Math.pow(PV.getData(i, 0),2);
        }
        PVr = Math.pow(PVr,0.5);
        for (int i = 0; i < 3; i++) {             		 // Нормированный вектор тяги
            ePV.setData(i,0,PV.getData(i, 0)/PVr);
        }

        double ra = 0;                                    // вектор визирования
        Matrix Va = new Matrix(1,3);
        Va.setData(0, 0, (targetCord[0] + nx - x));
        Va.setData(1, 0, (targetCord[1] + ny - y));
        Va.setData(2, 0, (targetCord[2] + nz - z));

        Matrix eVa = new Matrix(1,3);
        for (int i = 0; i < 3; i++) {
            double tmp = Va.getData(i, 0);
            ra += Math.pow(Va.getData(i, 0),2);
        }
        ra = Math.sqrt(ra);
        for (int i = 0; i < 3; i++) {             // Нормированиный вектор визирования
            eVa.setData(i, 0, Va.getData(0, i) / ra);
        }

        Matrix PG = new Matrix(1,3);
        PG.setData(0, 0, eVa.getData(0, 0));
        PG.setData(1, 0, eVa.getData(1, 0));
        PG.setData(2, 0, eVa.getData(2, 0));

    Matrix ePG = new Matrix(1,3);
    double PGr = 0;
    for (int i = 0; i < 3; i++) {
        PGr += Math.pow(PG.getData(i, 0),2);
    }
    PGr = Math.pow(PGr,0.5);
    for (int i = 0; i < 3; i++) {             // Нормированиный вектор тяги
        ePG.setData(i, 0, PG.getData(i, 0) / PGr); // по условию наведения
    }
    /*
    x3 =  y1*z2-y2*z1
    y3 =  z1*x2-z2*x1
    z3 =  x1*y2-x2*y1
     */
    Matrix omegaG = new Matrix(1,3);//CrossProduct().calc(ePV,ePG);  // ОмегаГ
    omegaG.setData(0, 0, ePV.getData(1, 0) * ePG.getData(2, 0) - ePV.getData(2, 0) * ePG.getData(1, 0));
    omegaG.setData(1, 0, ePV.getData(2, 0) * ePG.getData(0, 0) - ePV.getData(0, 0) * ePG.getData(2,0));
    omegaG.setData(2, 0, ePV.getData(0, 0) * ePG.getData(1, 0) - ePV.getData(1, 0) * ePG.getData(0,0));

                                                                      
    double omegaGr = 0;
    for (int i = 0; i < 3; i++) {
        omegaGr += Math.pow(omegaG.getData(i, 0),2);
    }
    omegaGr = Math.pow(omegaGr,0.5);
    for (int i = 0; i < 3; i++) {
        omegaG.setData(i, 0,omegaG.getData(0, i) * omegam / omegaGr);
    }

    double ax = (R[0]-(PV.getData(0, 0)))/m;     // Ускорение
    double ay = (R[1]+F1-(PV.getData(1, 0)))/m;
    double az = (R[2]-(PV.getData(2, 0)))/m;

    double ddelta = kDelta*rDelta/TDelta-delta/TDelta;   // СВ
    double dnx =  knx*rnx/Tnx-nx/Tnx;
    double dny =  kny*rny/Tny-ny/Tny;
    double dnz =  knz*rnz/Tnz-nz/Tnz;
    double dnvx =  knvx*rnvx/Tnvx-nvx/Tnvx;
    double dnvy =  knvy*rnvy/Tnvy-nvy/Tnvy;
    double dnvz =  knvz*rnvz/Tnvz-nvz/Tnvz;

    // Задержка угловой скорости
    double domegax = komega*omegaG.getData(0, 0)/Tomega-omegax/Tomega;
    double domegay = komega*omegaG.getData(1, 0)/Tomega-omegay/Tomega;
    double domegaz = komega*omegaG.getData(2, 0)/Tomega-omegaz/Tomega;

    Quaternion dquater = omegaQ.multiplication(quater); // Производная Кватерниона
    double dqx = -0.5*dquater.getIm(Quaternion.TYPE_COL).getData(0, 0);
    double dqy = -0.5*dquater.getIm(Quaternion.TYPE_COL).getData(1, 0);
    double dqz = -0.5*dquater.getIm(Quaternion.TYPE_COL).getData(2, 0);
    double dqphi = -0.5*dquater.getReal();

        args.setData(0, 0, vx);
        args.setData(1, 0, vy);
        args.setData(2, 0, vz);
        args.setData(3, 0, ax);
        args.setData(4, 0, ay);
        args.setData(5, 0, az);
        args.setData(6, 0, ddelta);
        args.setData(7, 0, dnx);
        args.setData(8, 0, dny);
        args.setData(9, 0, dnz);
        args.setData(10, 0, dnvx);
        args.setData(11, 0, dnvy);
        args.setData(12, 0, dnvz);
        args.setData(13, 0, dqx);
        args.setData(14, 0, dqy);
        args.setData(15, 0, dqz);
        args.setData(16, 0, dqphi);
        args.setData(17, 0, domegax);
        args.setData(18, 0, domegay);
        args.setData(19, 0, domegaz);

    return args;
    }
}
