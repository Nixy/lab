package com.edu.sys.models;

import com.edu.sys.matrix.Matrix;

import java.util.Random;

import static java.lang.Math.*;

/**
 * Created by Nixy on 28.05.14.
 */
public class Noise extends AbstractModel {

    Random r = new Random();
    double nu;
    double nu2;
    double nu3;
    double nu4;

    /*
    * Первый фильтр    угловой
    *             0.1
    *   W(p)=----------------
    *           1*p + 1
    *
    * Второй фильтр      дальность
    *             1
    *   W(p)=----------------
    *           1*p + 1
    * */

    double K1 = 0.005;
    double T1 = 0.05;

    double K2 = 0.5;
    double T2 = 0.05;

    public Noise(Matrix init,double step){
        super(init,step);
        nu = r.nextGaussian()/sqrt(step);
        nu2 = r.nextGaussian()/sqrt(step);
        nu3 = r.nextGaussian()/sqrt(step);
        nu4 = r.nextGaussian()/sqrt(step);

    }

    @Override
    public boolean isEnough(Object... params) {
        return false;
    }

    @Override
    public Matrix F(double t, Matrix Xn) {
        double xid = Xn.getData(0,0);
        double xi2 = Xn.getData(1,0);
        double xi3 = Xn.getData(2,0);
        double xi1 = Xn.getData(3,0);

        double dxi1 = -1./T1*xi1+K1/T1*nu;
        double dxi2 = -1./T1*xi2+K1/T1*nu2;
        double dxi3 = -1./T1*xi3+K1/T1*nu3;
        double dxid = -1./T2*xid+K2/T2*nu4;

        Xn.setData(0, 0, dxid);
        Xn.setData(1, 0, dxi2);
        Xn.setData(2, 0, dxi3);
        Xn.setData(3, 0, dxi1);

        return Xn;
    }

    @Override
    public void setX(Matrix X) {
        super.setX(X);
        nu = r.nextGaussian()/sqrt(step);
        nu2 = r.nextGaussian()/sqrt(step);
        nu3 = r.nextGaussian()/sqrt(step);
        nu4 = r.nextGaussian()/sqrt(step);

    }
}
