package com.edu.sys.filters;

import com.edu.sys.matrix.Matrix;
import com.edu.sys.matrixoperations.Add2;
import com.edu.sys.matrixoperations.Multi;
import com.edu.sys.matrixoperations.Transpose;
import com.edu.sys.models.Simulator;

import static java.lang.Math.pow;

/**
 * Created by Nixy on 24.05.14.
 */
public class AngleDiscretFilter extends DiscretKalmanFilter {

     /*
    *   X = (fiY,fiZ,omegaY,omegaZ}
    *
    *   d fiX /dt = omegaX
    *   d fiY /dt = omegaY
    *   d fiZ /dt = omegaZ
    *   d omegaX /dt = xiX
    *   d omegaY /dt = xiY
    *   d omegaZ /dt = xiZ
    *   d xiX /dt = 0
    *   d xiY /dt = 0
    *   d xiZ /dt = 0
    *
    *   Y = {fiY,fiZ,omegaY,omegaZ)
    *
    * */

    int SIZE = 9;
    int MEASURE = 6;
    Simulator simulator;
    double step;
    int t;

    public AngleDiscretFilter(Simulator simulator){
        this.simulator = simulator;
        step = simulator.step;
        getDispEta();
        getDispXi();
        getU();
        getH();
        getF();
        Xo = new Matrix(SIZE,1);
        Ko =  new Matrix(new double[][]{{1,0,0,step,0,0,pow(step, 2)/2,0,0},
                {0,1,0,0,step,0,0,pow(step,2)/2,0},
                {0,0,1,0,0,0,step,0,0,pow(step,2)/2},
                {0,0,0,1,0,0,step,0,0},
                {0,0,0,0,1,0,0,step,0},
                {0,0,0,0,0,1,0,0,step},
                {0,0,0,0,0,0,1,0,0},
                {0,0,0,0,0,0,0,1,0},
                {0,0,0,0,0,0,0,0,1}});
        Ko = Add2.calc(Ko, Transpose.calc(Ko));
        XoList.add(Xo);
        KoList.add(Ko);
        for (t = 0 ; t < simulator.Y.size()-1; t++){
            getXf();
            getKf();
            getC();
            getKo();
            getXo();
        }
    }

    @Override
    protected void getDispXi() {
        DispXi = new Matrix(SIZE,SIZE);
    }

    @Override
    protected void getDispEta() {
        DispEta = Matrix.E(MEASURE);
    }

    @Override
    protected void getdK() {
        dK = new Matrix(SIZE,SIZE);
    }

    @Override
    protected void getU() {
        U = new Matrix(SIZE,1);
    }

    @Override
    protected void getF() {    double step =0;
        F = new Matrix(new double[][]{{1,0,0,step,0,0,pow(step, 2)/2,0,0},
                {0,1,0,0,step,0,0,pow(step,2)/2,0},
                {0,0,1,0,0,step,0,0,pow(step,2)/2},
                {0,0,0,1,0,0,step,0,0},
                {0,0,0,0,1,0,0,step,0},
                {0,0,0,0,0,1,0,0,step},
                {0,0,0,0,0,0,1,0,0},
                {0,0,0,0,0,0,0,1,0},
                {0,0,0,0,0,0,0,0,1}});
    }

    @Override
    protected void getH() {
        double step =0;
        H  = new Matrix(new double[][]{{1,0,0,step,0,0,pow(step, 2)/2,0,0},
                                        {0,1,0,0,step,0,0,pow(step,2)/2,0},
                                        {0,0,1,0,0,step,0,0,pow(step,2)/2},
                                        {0,0,0,1,0,0,step,0,0},
                                        {0,0,0,0,1,0,0,step,0},
                                        {0,0,0,0,0,1,0,0,step}});
    }

    @Override
    protected Matrix getY() {
        Matrix Y = simulator.Y.get(t);
        Matrix Angle = Y.getVector(0,1,MEASURE+1,Matrix.TYPE_COL);
        return Angle;
    }

    @Override
    protected Matrix getG() {
        return Multi.calc(H, Xf);
    }
}
