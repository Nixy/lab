package com.edu.sys.filters;

import com.edu.sys.matrix.Matrix;
import com.edu.sys.matrixoperations.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nixy on 24.05.14.
 */
public abstract class DiscretKalmanFilter {
    /*       Xo - оценка
    *        Ko - ковариация ошибок оценки
    *        Xf - прогноз
    *        Kf - ковариация ошибок прогноза        *
    *        С - коэффициент поправки
    *        dK - матрица поправки в ковариации
    *
    *        F - фундументальная матрица
    *        H - матрица наблюдаймости
    *        U - неслучайное воздействие
    *
    *        DispXi - дисперсии сдучайных воздействий
    *        DispZet - дисперсии ошибок измерений , должна быть заранее обращена
    *
    * */


    protected Matrix Xo;
    protected Matrix Ko;
    protected Matrix Xf;
    protected Matrix Kf;
    protected Matrix C;
    protected Matrix U;
    protected Matrix F;
    protected Matrix H;
    protected Matrix DispXi;
    protected Matrix DispEta;
    protected Matrix dK;

    public List<Matrix> XoList = new ArrayList<Matrix>();
    public List<Matrix> KoList = new ArrayList<Matrix>();
    public List<Matrix> XfList = new ArrayList<Matrix>();
    public List<Matrix> KfList = new ArrayList<Matrix>();
    public List<Matrix> DList = new ArrayList<Matrix>();
    public List<Matrix> CList = new ArrayList<Matrix>();


    protected abstract void getDispXi();
    protected abstract void getDispEta();
    protected abstract void getU();
    protected abstract void getdK();

    protected abstract void getF();
    protected abstract void getH();
    protected abstract Matrix getY();
    protected abstract Matrix getG();


    protected void getXo() {

        Matrix tmp2 = Sub2.calc(getY(), getG());
        Matrix tmp3 = Multi.calc(C,tmp2);
        Xo = Add2.calc(Xf,tmp3);
        DList.add(tmp2);
        XoList.add(Xo);
    }

    protected void getC(){
        Matrix HT = Transpose.calc(H);
        Matrix tmp = Multi.calc(H,Kf);
        tmp = Multi.calc(tmp,HT);
        tmp = Add2.calc(tmp,DispEta);
        tmp = GaussInv.calc(tmp);
        tmp = Multi.calc(HT,tmp);
        C = Multi.calc(Kf,tmp);
        CList.add(C);
    }

    protected  void getKo(){
        Matrix tmp = Multi.calc(C,H);
        tmp = Sub2.calc(Matrix.E(tmp.getRow()),tmp);
        Ko = Multi.calc(tmp,Kf);
        KoList.add(Ko);
    };

    protected  void getKf(){
        Matrix FT = Transpose.calc(F);
        Matrix tmp = Multi.calc(F,Ko);
        MultiWith.calc(tmp,FT);
        Kf = Add2.calc(tmp,DispXi);
        GaussInv.calc(Kf);
        Kf = Add2.calc(Kf,dK);
        KfList.add(Kf);
    };

    protected  void getXf(){
        Xf = Multi.calc(F,Xo);
        Xf = Add2.calc(Xf,U);
        XfList.add(Xf);
    };

}
