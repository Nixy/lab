package com.edu.sys.filters;

import com.edu.sys.matrix.Matrix;
import com.edu.sys.matrixoperations.*;
import com.edu.sys.models.Simulator;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

/**
 * Created by Nixy on 24.05.14.
 */
public class RangeDiscretFilter extends DiscretKalmanFilter {
    Simulator simulator;
    double step;
    int t;

    /*
    *   X = (d0,d1,d2,d3,d4}
    *
    *   d d0 /dt = d1
    *   d d1 /dt = d2
    *   d d2 /dt = d3
    *   d d3 /dt = d4
    *   d d4 /dt = 0
    *
    *   Y = {d0+d1*t+d2*t^2/2+d3*t^3/6+d4*t^4/24)
    *
    * */

    public static final int SIZE = 5;
    public static final int MEASURE = 1;

    public RangeDiscretFilter(Simulator simulator){
        this.simulator = simulator;
        step = simulator.step;
        getDispEta();
        getDispXi();
        getU();
        getH();
        getF();
        Xf = new Matrix(SIZE,1);

        Kf = new Matrix(new double[][]{{1e1,step,pow(step,2)/2,pow(step,3)/6,pow(step,4)/24},
                {0,1,step,pow(step,2)/2,pow(step,3)/6},
                {0,0,1,step,pow(step,2)/2},
                {0,0,0,1,step},
                {0,0,0,0,1}});
        Kf = Add2.calc(Kf, Transpose.calc(Kf));
        Kf = Kf.getWithFactor(1e3);
        XoList.add(Xf);
        KoList.add(Kf);
        for (t = 0 ; t < simulator.Y.size(); t++){
            getC();
            getKo();
            getXo();
            getXf();
            getKf();

        }
    }
     @Override
    protected void getDispXi() {
       //  DispXi = Matrix.E(SIZE);
         DispXi = new Matrix(SIZE,SIZE);
    }

    @Override
    protected void getDispEta() {
        DispEta = Matrix.E(MEASURE);
    }



    @Override
    protected void getU() {
        U = new Matrix(SIZE,1);
    }

    @Override
    protected void getdK() {
        dK = new Matrix(SIZE,SIZE);
    }

    @Override
    protected void getF() {
        F = new Matrix(new double[][]{{1,step,pow(step,2)/2,pow(step,3)/6,pow(step,4)/24},
                {0,1,step,pow(step,2)/2,pow(step,3)/6},
                {0,0,1,step,pow(step,2)/2},
                {0,0,0,1,step},
                {0,0,0,0,1}});
    }

    @Override
    protected void getH() {

        H = new Matrix(new double[][]{{1,step,pow(step,2)/2,pow(step,3)/6,pow(step,4)/24}});
    }

    @Override
    protected Matrix getY() {
        Matrix Y = simulator.Y.get(t);
        Matrix D = Y.getVector(0,0,1,Matrix.TYPE_COL);
        return D;
    }

    @Override
    protected Matrix getG() {
        return Multi.calc(H, Xf);
    }

    double fact(int n){
        if (n == 0) return 1;
        return n * fact(n-1);
    }

    public double getSigma(int n){

        double sigma = 0;
        Matrix K = KoList.get(n);
        for(int i  = 0; i < K.getRow(); i++ )
            for(int j  = 0; j < K.getCol(); j++ ){
                //double t = n*step;
                double t = step;
                double a1 =  pow(t,i)/fact(i);
                double a2 =  pow(t,j)/fact(j);
                sigma += K.getData(i,j)*K.getData(i,j)*a1*a2;
            }
        return sqrt(sigma);
    }
}
