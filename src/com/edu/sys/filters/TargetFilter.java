package com.edu.sys.filters;

import com.edu.sys.matrix.Matrix;
import com.edu.sys.matrixoperations.Add2;
import com.edu.sys.matrixoperations.Multi;
import com.edu.sys.matrixoperations.Transpose;
import com.edu.sys.models.Simulator;

import static java.lang.Math.cos;
import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

/**
 * Created by Nixy on 26.05.14.
 */
public class TargetFilter extends DiscretKalmanFilter {

    /*
    * dx = vx
    * dy = vy
    * dz = vz
    * dvx = 0
    * dvy = 0
    * dvz = 0
    * */
    Simulator simulator;
    double step;
    int t;

    public static final int SIZE = 9;
    public static final int MEASURE = 3;

    public TargetFilter(Simulator simulator){
        this.simulator = simulator;
        step = simulator.step;
        getDispXi();
        getU();
        getH();
        getF();
        getdK();
        Xo = new Matrix(new double[][]{{0},{0},{1000},{0},{0},{0},{0},{0},{0}});
        Ko = Matrix.E(SIZE);
        Ko = Ko.getWithFactor(1e4);
        Ko = Multi.calc(Ko,Transpose.calc(Ko));
        XoList.add(Xo);
        KoList.add(Ko);

        for (t = 0 ; t < simulator.Y.size(); t++){
            getDispEta();
            getXf();
            getKf();
            getC();
            getKo();
            getXo();
        }

    }

     @Override
     protected void getDispXi() {
         DispXi = Matrix.E(SIZE).getWithFactor(0);
     }

    @Override
    protected void getDispEta() {
        double d = sqrt(pow(Xo.getData(0,0),2)+pow(Xo.getData(1,0),2)+pow(Xo.getData(2,0),2));
        DispEta = new Matrix(new double[][]{{(d+2.5)*cos(2.5E-4),0,0},
                                             {0,(d+2.5)*cos(2.5E-4),0},
                                              {0,0,(d+2.5)*cos(2.5E-4)}});
        DispEta = DispEta.getWithFactor(1);
    }


    @Override
    protected void getU() {
        U = new Matrix(SIZE,1);
    }

    @Override
    protected void getdK() {
        /*dK =  new Matrix(new double[][]{{0,0,0,1*step,0,0},
                {0,0,0,0,1*step,0},
                {0,0,0,0,0,1*step},
                {0,0,0,3000,0,0},
                {0,0,0,0,3000,0},
                {0,0,0,0,0,3000}});*/
        dK =  new Matrix(new double[][]{{0,0,0,1*step,0,0,step*step/2,0,0},
                                        {0,0,0,0,1*step,0,0,step*step/2,0},
                                        {0,0,0,0,0,1*step,0,0,step*step/2},
                                        {0,0,0,100,0,0,step,0,0},
                                        {0,0,0,0,100,0,0,step,0},
                                        {0,0,0,0,0,100,0,0,step},
                                        {0,0,0,0,0,0,1000,0,0},
                                        {0,0,0,0,0,0,0,1000,0},
                                        {0,0,0,0,0,0,0,0,1000}});
        dK = Add2.calc(dK, Transpose.calc(dK));
        dK = dK.getWithFactor(0.005);

    }

    @Override
    protected void getF() {

      /*  F =  new Matrix(new double[][]{{1,0,0,step,0,0},
                                       {0,1,0,0,step,0},
                                       {0,0,1,0,0,step},
                                       {0,0,0,1,0,0},
                                       {0,0,0,0,1,0},
                                       {0,0,0,0,0,1}});*/
      F = new Matrix(new double[][]{{1,0,0,step,0,0,step*step/2,0,0},
                                    {0,1,0,0,step,0,0,step*step/2,0},
                                    {0,0,1,0,0,step,0,0,step*step/2},
                                    {0,0,0,1,0,0,step,0,0},
                                    {0,0,0,0,1,0,0,step,0},
                                    {0,0,0,0,0,1,0,0,step},
                                    {0,0,0,0,0,0,1,0,0},
                                    {0,0,0,0,0,0,0,1,0},
                                    {0,0,0,0,0,0,0,0,1}});


    }

    @Override
    protected void getH() {

        /*H = new Matrix(new double[][]{{1.,0,0,step,0,0},
                                      {0,1.,0,0,step,0},
                                      {0,0,1.,0,0,step}});*/

        H = new Matrix(new double[][]{{1,0,0,0,0,0,0,0,0},
                                      {0,1,0,0,0,0,0,0,0},
                                      {0,0,1,0,0,0,0,0,0}});
    }

    @Override
    protected Matrix getY() {
        Matrix Y = simulator.Y.get(t);
        Matrix D = Y.getVector(0,7,10,Matrix.TYPE_COL);
        return D;
    }

    @Override
    protected Matrix getG() {
        return Multi.calc(H, Xf);
    }
}
