package com.edu.sys.filters;



import com.edu.sys.functions.Function;
import com.edu.sys.matrix.Matrix;
import com.edu.sys.matrixoperations.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: Nixy
 * Date: 23.10.13
 * Time: 9:04
 * To change this template use File | Settings | File Templates.
 */
public abstract class AbstractMNK {

    protected Function[] FI;


    protected Random random = new Random(System.currentTimeMillis());
    protected Matrix y ;
    protected Matrix fi ;
    protected Matrix dispI ;
    protected Matrix H ;
    protected Matrix X1 = new Matrix(4,1);
    protected List<Matrix> K = new ArrayList<Matrix>();
    protected List<Matrix> aX1 = new ArrayList<Matrix>();

    public List<Matrix> getK() {
        return K;
    }

    public List<Matrix> getaX1() {
        return aX1;
    }

    public AbstractMNK(Function[] fi){
        FI = fi;
    }

    public abstract Matrix getY ();

    public abstract Matrix getFi(Matrix X);

    public abstract Matrix dispI();

    public abstract Matrix getH(Matrix X);

    protected boolean compare (double[] E, Matrix A, Matrix B ){
        for (int i = 0; i < A.getRow(); i++)
            for (int j = 0; j < A.getCol(); j++)
                if (Math.abs(A.getData(i,j)-B.getData(i,j)) > E[i])
                    return false;
        return true;
    }

    public Matrix getX(Matrix X,double[] e){

        X1.copy(X);
        getY();
        Matrix HT = Transpose.calc(getH(X));
        dispI();
        getFi(X);
        aX1.add(X1);
        K.add(new Matrix(H.getRow(),H.getRow()));
        int count = 0;
        do{
            X.copy(X1);
            Matrix tmp2 = Multi.calc(HT, dispI);
            Matrix tmp3 = Multi.calc(tmp2,H);
            Matrix k = CInverse.calc(tmp3);
            Matrix k1 = GaussInv.calc(tmp3);
            K.add(k);
            if (k1 == null)
                break;
            Matrix tmp4 = Multi.calc(k, HT);
            Matrix tmp5 = Multi.calc(tmp4, dispI);

            Matrix dy = Sub2.calc(y, fi);

            Matrix tmp6 = MultiWith.calc(tmp5, dy);
            X1 = Add2.calc(X1,tmp6);
            aX1.add(X1);
            getFi(X1);
            HT = Transpose.calc(getH(X1));
            count++;
            if(count > 1000)
                break;
        }
        //while (true);
        while (!compare(e,X1,X));

        return X1;

    }
}
