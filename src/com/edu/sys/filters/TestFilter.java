package com.edu.sys.filters;

import com.edu.sys.matrix.Matrix;
import com.edu.sys.matrixoperations.Add2;
import com.edu.sys.matrixoperations.Multi;
import com.edu.sys.matrixoperations.Transpose;
import com.edu.sys.models.Simulator;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

/**
 * Created by Nixy on 26.05.14.
 */
public class TestFilter extends DiscretKalmanFilter {
    /*
   * dx = vx
   * dy = vy
   * dz = vz
   * dvx = 0
   * dvy = 0
   * dvz = 0
   * */
    Simulator simulator;
    double step;
    int t;

    public static final int SIZE = 9;
    public static final int MEASURE = 3;

    public TestFilter(Simulator simulator){
        this.simulator = simulator;
        step = simulator.step;
        getDispEta();
        getDispXi();
        getU();
        getH();
        getF();
        getdK();
        Xf = new Matrix(SIZE,1);
        Kf = Multi.calc(F, Transpose.calc(F));
        Kf = Kf.getWithFactor(1e3);
        XfList.add(Xf);
        KfList.add(Kf);
        for (t = 0 ; t < simulator.Y.size(); t++){
            getC();
            getKo();
            getXo();
            getXf();
            getKf();
        }
    }

    @Override
    protected void getDispXi() {
        DispXi = new Matrix(SIZE,SIZE);
    }

    @Override
    protected void getDispEta() {
        DispEta = new Matrix(new double[][]{{5000/(t+1),0,0},
                                            {0,5000/(t+1),0},
                                            {0,0,5000/(t+1)}});
    }

    @Override
    protected void getdK() {
        dK =  new Matrix(new double[][]{{1,0,0,step,0,0,step*step/2,0,0},
                                        {0,1,0,0,step,0,0,step*step/2,0},
                                        {0,0,1,0,0,step,0,0,step*step/2},
                                        {0,0,0,100,0,0,step,0,0},
                                        {0,0,0,0,100,0,0,step,0},
                                        {0,0,0,0,0,100,0,0,step},
                                        {0,0,0,0,0,0,1000,0,0},
                                        {0,0,0,0,0,0,0,1000,0},
                                        {0,0,0,0,0,0,0,0,1000}});
        dK = Add2.calc(dK, Transpose.calc(dK));
        dK = dK.getWithFactor(0.008);
    }

    @Override
    protected void getU() {
        U = new Matrix(SIZE,1);
    }

    @Override
    protected void getF() {
       /* F =  new Matrix(new double[][]{{1,0,0,step,0,0},
                {0,1,0,0,step,0},
                {0,0,1,0,0,step},
                {0,0,0,1,0,0},
                {0,0,0,0,1,0},
                {0,0,0,0,0,1}});*/
        F = new Matrix(new double[][]{{1,0,0,step,0,0,step*step/2,0,0},
                                      {0,1,0,0,step,0,0,step*step/2,0},
                                      {0,0,1,0,0,step,0,0,step*step/2},
                                      {0,0,0,1,0,0,step,0,0},
                                      {0,0,0,0,1,0,0,step,0},
                                      {0,0,0,0,0,1,0,0,step},
                                      {0,0,0,0,0,0,1,0,0},
                                      {0,0,0,0,0,0,0,1,0},
                                      {0,0,0,0,0,0,0,0,1}});
    }

    @Override
    protected void getH() {
        H = new Matrix(new double[][]{{1.,0,0,step,0,0,0,0,0},
                {0,1.,0,0,step,0,0,0,0},
                {0,0,1.,0,0,step,0,0,0}});
    }

    @Override
    protected Matrix getY() {
        Matrix Y = simulator.Y.get(t);
        Matrix D = Y.getVector(0,7,10,Matrix.TYPE_COL);
        return D;
    }

    @Override
    protected Matrix getG() {
        return Multi.calc(H, Xf);
    }
}
