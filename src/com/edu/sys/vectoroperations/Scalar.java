package com.edu.sys.vectoroperations;

import com.edu.sys.matrix.Matrix;

/**
 * Created by Nixy on 06.05.14.
 */
public class Scalar {
    private static boolean isVector(Matrix m){
        if ((m.getRow() > 1 && m.getCol() == 1)) return true; else  return false;
    }

    public static double calc(Matrix... args){
        if (args.length > 0 && isVector(args[0]) && isVector(args[1]) ){
            double sum = 0;
            for (int i = 0; i < args[0].getRow(); i++){
                sum += args[0].getData(i,0)*args[1].getData(i,0);
            }
            return sum;
        }
        return  Double.NaN;
    }
}
