package com.edu.sys.vectoroperations;

import com.edu.sys.matrix.Matrix;
import com.edu.sys.matrixoperations.Determinant;

/**
 * Created by Nixy on 30.04.14.
 */
public class CrossProduct  {

    private static boolean isVector(Matrix m){
        if ((m.getRow() > 1 && m.getCol() == 1)) return true; else  return false;
    }

    public static Matrix calc(Matrix... args){
        if (args.length > 1 && isVector(args[0]) && isVector(args[1])){
            double i = Determinant.calc(new Matrix(new double[][]{{args[0].getData(1, 0), args[0].getData(2, 0)},
                    {args[1].getData(1, 0), args[1].getData(2, 0)}}));
            double j = -1*Determinant.calc(new Matrix(new double[][]{{args[0].getData(0,0),args[0].getData(2,0)},
                                                                  {args[1].getData(0,0),args[1].getData(2,0)}}));
            double k = Determinant.calc(new Matrix(new double[][]{{args[0].getData(0,0),args[0].getData(1,0)},
                                                                  {args[1].getData(0,0),args[1].getData(1,0)}}));
            return new Matrix(new double[][]{{i},{j},{k}});
        }
        return  null;
    }
}
