package com.edu.sys.vectoroperations;

import com.edu.sys.matrix.Matrix;

/**
 * Created by Nixy on 30.04.14.
 */
public class Norm {
    private static boolean isVector(Matrix m){
        if ((m.getRow() > 1 && m.getCol() == 1)) return true; else  return false;
    }

    public static Matrix calc(Matrix... args){
        if (args.length > 0 && isVector(args[0]) ){
            double module = Module.calc(args[0]);
            if (module != 0){
                Matrix result = new Matrix(args[0].getRow(),1);
                for (int i = 0; i < result.getRow(); i ++){
                    result.setData(i,0,args[0].getData(i,0)/module);
                }
                return result;
            }   else {
                return args[0];
            }
        }
        return  null;
    }
}
