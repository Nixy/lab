package com.edu.sys.vectoroperations;

import com.edu.sys.matrix.Matrix;


/**
 * Created by Nixy on 30.04.14.
 */
public class Module {
    private static boolean isVector(Matrix m){
        if ((m.getRow() > 1 && m.getCol() == 1)) return true; else  return false;
    }

    public static double calc(Matrix... args){
        if (args.length > 0 && isVector(args[0]) ){
            double sum = 0;
            for (int i = 0; i < args[0].getRow(); i++){
                sum += Math.pow(args[0].getData(i,0),2);
            }
            return Math.sqrt(sum);
        }
        return  Double.NaN;
    }
}
