package com.edu.sys.functions;


import com.edu.sys.matrix.Matrix;

/**
 * Created with IntelliJ IDEA.
 * User: Миша
 * Date: 08.10.13
 * Time: 21:04
 * To change this template use File | Settings | File Templates.
 */
public interface Function {
    public double calc(Matrix... args);

}
