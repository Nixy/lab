package com.edu.gui.part2;

import javax.swing.*;

/**
 * Author Grinch
 * Date: 14.06.14
 * Time: 20:09
 */
public class Main {
    private JPanel panel1;
    JMenuBar menuBar;
    JMenu menu, submenu;
    JMenuItem menuItem;
    private JList list1;
    private JButton button1;
    private JButton button2;
    private JPanel panel2;

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Main");
        frame.setContentPane(new Main().panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
