package com.edu.gui;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;

import javax.swing.*;
import java.awt.*;

/**
 * Author Grinch
 * Date: 08.05.14
 * Time: 9:49
 */
public class Test extends ApplicationFrame {

    JPanel p1,p2,p3,p4,mainPanel;
    Dimension d;

    /**
     * Создаёт новый фрейм с графиком
     * @param title Заголовок окна
     */
    public Test(String title){
        super(title);
        JTabbedPane tabs = new JTabbedPane();
        setLayout(new GridLayout(1,1));     //Layout главного окна - таблица. Если нужно паравозиком - FlowLayout
        p1=new JPanel(new GridBagLayout()); //создаем панельки
        p2=new JPanel(new GridBagLayout());
        p3=new JPanel(new GridBagLayout());
        p4=new JPanel(new GridBagLayout());
        mainPanel=new JPanel(new GridLayout(2,2));
        mainPanel.add(p1);
        mainPanel.add(p2);
        mainPanel.add(p3);
        mainPanel.add(p4);
        tabs.add("Графичек 1",mainPanel);
        tabs.add("Вторая вкладочка",new JPanel());

        d = new Dimension(300,300); //размеры панелек

        p1.setPreferredSize(d);
        p2.setPreferredSize(d);
        p3.setPreferredSize(d);
        p4.setPreferredSize(d);

        add(tabs, BorderLayout.CENTER);

        final XYDataset dataset = createDataset(); //данные для графика
        final JFreeChart chart = createChart(dataset,"Тестовый графичек"); //сам график
        final ChartPanel chartPanel = new ChartPanel(chart); //добавляем график на Swing'оподдерживаемую панельку
        final ChartPanel chartPanel2 = new ChartPanel(chart);
        final ChartPanel chartPanel3 = new ChartPanel(chart);
        final ChartPanel chartPanel4 = new ChartPanel(chart);
        chartPanel.setPreferredSize(d); //размеры = размерам панелек(на всю панельку)
        chartPanel2.setPreferredSize(d);
        chartPanel3.setPreferredSize(d);
        chartPanel4.setPreferredSize(d);
        p1.add(chartPanel);  //добавляем графички на панель
        p2.add(chartPanel2);
        p3.add(chartPanel3);
        p4.add(chartPanel4);
        //setContentPane(chartPanel);
    }

    /**
     * Наполняет Set данными для построения графика
     * @return Данные для построения
     */
    private static XYDataset createDataset() {
        final XYSeries series1 = new XYSeries("First");
        series1.add(1.0, 1.0);
        series1.add(2.0, 4.0);
        series1.add(3.0, 3.0);
        series1.add(4.0, 5.0);
        series1.add(5.0, 5.0);
        series1.add(6.0, 7.0);
        series1.add(7.0, 7.0);
        series1.add(8.0, 8.0);

        final XYSeries series2 = new XYSeries("Second");
        series2.add(1.0, 5.0);
        series2.add(2.0, 7.0);
        series2.add(3.0, 6.0);
        series2.add(4.0, 8.0);
        series2.add(5.0, 4.0);
        series2.add(6.0, 4.0);
        series2.add(7.0, 2.0);
        series2.add(8.0, 1.0);

        final XYSeries series3 = new XYSeries("Third");
        series3.add(3.0, 4.0);
        series3.add(4.0, 3.0);
        series3.add(5.0, 2.0);
        series3.add(6.0, 3.0);
        series3.add(7.0, 6.0);
        series3.add(8.0, 3.0);
        series3.add(9.0, 4.0);
        series3.add(10.0, 3.0);

        final XYSeriesCollection dataset = new XYSeriesCollection();
        dataset.addSeries(series1);
        dataset.addSeries(series2);
        dataset.addSeries(series3);

        return dataset;
    }

    /**
     * Creates a chart.
     *
     * @param dataset  the data for the chart.
     *
     * @return a chart.
     */
    private JFreeChart createChart(final XYDataset dataset, String title) {

        // create the chart...
        final JFreeChart chart = ChartFactory.createXYLineChart(
                title,      // chart title
                "X",                      // x axis label
                "Y",                      // y axis label
                dataset,                  // data
                PlotOrientation.VERTICAL,
                true,                     // include legend
                true,                     // tooltips
                false                     // urls
        );

        // NOW DO SOME OPTIONAL CUSTOMISATION OF THE CHART...
        chart.setBackgroundPaint(Color.white);

//        final StandardLegend legend = (StandardLegend) chart.getLegend();
        //      legend.setDisplaySeriesShapes(true);

        // get a reference to the plot for further customisation...
        final XYPlot plot = chart.getXYPlot();
        plot.setBackgroundPaint(Color.lightGray);
        //    plot.setAxisOffset(new Spacer(Spacer.ABSOLUTE, 5.0, 5.0, 5.0, 5.0));
        plot.setDomainGridlinePaint(Color.white);
        plot.setRangeGridlinePaint(Color.white);

        final XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
        renderer.setSeriesLinesVisible(0, false);
        renderer.setSeriesShapesVisible(1, false);
        plot.setRenderer(renderer);

        // change the auto tick unit selection to integer units only...
        final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        // OPTIONAL CUSTOMISATION COMPLETED.

        return chart;
    }

    public static void main(final String[] args) {

        final Test demo = new Test("Line Chart Demo 6");
        demo.pack();
        RefineryUtilities.centerFrameOnScreen(demo);
        demo.setVisible(true);

    }
}
